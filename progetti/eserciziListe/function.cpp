#include "function.h"

void saluta(){
    cout<<"Classe di alunni con voti "<<endl;
}

void stampa(Alunno a){
    cout<<"====== Alunno ==========="<< endl;
    cout<<"Nome: "<<a.nome<<endl<<"Cognome: "<<a.cognome<<endl<<"Anno: "<<a.anno<<endl<<"Classe: "<<a.classe<<endl;
    cout<<"====== Voti ============="<<endl;
    stampa(a.voti);
}

void stampa(Voto v){
    cout<<"Materia: "<<v.materia<<"; Data: "<<v.data<<"; Voto: "<<v.numero;
}

void stampa(const ListaVoti * l){
    if(l!= NULL){
        stampa(l->dato);
        stampa(l->next);
    }else{
        //cout<<"fine lista"<<endl;
    }
}

void stampa(const ListaAlunni * l){
    if(l!= NULL){
        stampa(l->dato);
        stampa(l->next);
    }else{
        //cout<<"fine lista"<<endl;
    }
}

void stampa(Domanda){
    cout<<"Domanda: "<< d.domanda<<"; Voto: "<<d.voto<<endl;
}

void stampa(ListaDomande){
//    ListaDomande d;
    for(int i=0; i<d.size();i++){
        cout<<d[i].domanda<<d[i].voto<<endl;
    }
}

string getNome(){
    string ans = "placeholder";
    string tmp[]={"Marco", "Mattia", "Matteo", "Valerio", "Pietro"};
    ans=tmp[casualeTra(0, sizeof(tmp)-1)];
    return ans;
}

string getCognome(){
    string ans = "placeholder";
    string tmp[]={"Rosso", "Pesci", "Costa", "Verdi", "Neri"};
    ans=tmp[casualeTra(0, sizeof(tmp)-1)];
    return ans;
}

string getClasse(){
    string ans = "placeholder";
    string tmp[]={"1aIT", "2aIT", "3aIT", "4aIT", "5aIT"};
    ans=tmp[casualeTra(0, sizeof(tmp)-1)];
    return ans;
}

string getData(){
    int giorno=rand()%30+1;
    int mese=rand()%12+1;
    if(mese==2 && giorno>28){
        giorno=rand()%27+1;
    }
    return to_string(giorno) + "/" + to_string(mese) + "/" + to_string(casualeTra(2000, 2024));
}

string getMateria(){
    string ans = "placeholder";
    string tmp[]={"Matematica", "Informatica", "Italiano", "Storia", "Inglese"};
    ans=tmp[casualeTra(0, sizeof(tmp)-1)];
    return ans;
}

int getAnno(){
    int ans= casualeTra(1960, 2010);
}

double getNumero(){
    double ans= casualeTra(2, 10);
    return ans;
}

Alunno getAlunno(){
    Alunno ans;
    ans.nome= getNome();
    ans.cognome=getCognome();
    ans.anno=getAnno();
    ans.classe=getClasse();
    ans.voti=getListaVoti();
    return ans;
}

Voto getVoto(){
    Voto ans;
    ans.materia=getMateria();
    ans.data=getData();
    ans.numero=getNumero();
    return ans;
}

string getDomandaFatta(){
    string ans;
    string tmp[]= {"Come ti chiami?", "Quando è nato napoleone", "Di che colore era il cavallo bianco di napoleone", "Che verso fa il coccodrilo"};
    ans =tmp[casualeTra(0, sizeof(tmp)/sizeof(tmp[0])-1)];
    return ans;
}

Domanda getDomanda(){
    Domanda ans;
    ans.domanda=getDomandaFatta();
    ans.voto=getNumero();
    return ans;
}

ListaVoti * getListaVoti(){
    ListaVoti * ans=NULL, *tmp;
    ans=new (ListaVoti);
    ans->dato= getVoto();
    ans-> next=NULL;
    tmp=ans;
    for(int i=0; i<casualeTra(5,10);i++){
        tmp->next=new(ListaVoti);
        tmp=tmp->next;
        tmp->dato=getVoto();
        tmp->next=NULL;
    }
}

ListaAlunni * getListaAlunni(){
    ListaAlunni * ans=NULL, *tmp;
    ans=new (ListaAlunni);
    ans->dato= getAlunno();
    ans-> next=NULL;
    tmp=ans;
    for(int i=0; i<casualeTra(5,10);i++){
        tmp->next=new(ListaAlunni);
        tmp=tmp->next;
        tmp->dato=getAlunno();
        tmp->next=NULL;
    }
}

ListaDomande getListaDomande(){
    ListaDomande ans=NULL, tmp;
    ans=new (ListaDomande);
    ans->domanda=getDomanda();
    ans->voto=getVoto();
    tmp=ans;
    for(int i=0; i<casualeTra(5,10);i++){
        tmp->domanda=getDomanda();
        ans->voto=getVoto();
        ListaDomande.push_back(tmp);
        /*tmp->next=new(ListaDomande);
        tmp=tmp->next;
        tmp->dato=getDomanda();
        tmp->
        tmp->next=NULL;*/
    }
}

int casualeTra(int a, int b){
    return (rand()%(a-b+1)+b);
}