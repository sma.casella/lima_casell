#include "function.h"

string add_connessione(nodo precedente, nodo successivo){
    string tmp=" ";
    tmp= precedente.carattere+"--->"+ successivo.carattere;
    return tmp;
}
void crea_file(string nome_file){
    string nf=nome_file;
    ofstream wfile(nf);
    wfile<<"einstein";
    wfile.close();
}
bool esiste(string nome_file){
    bool ans=1;
    ifstream rfile;
    nome_file+=".md";
    rfile.open(nome_file);
    if(rfile.is_open()){
        cout<<"il file esiste gia', lo leggo."<<endl;
        leggi_file(nome_file);
        ans=0;
    }else{
        cout<<"il file non esiste, creo il file"<<endl;
        crea_file(nome_file);
        cout<<"."<<endl<<"."<<endl<<"."<<endl<<"il file e' stato creato"<<endl;
        ans=1;
    }
    return ans;
}

void leggi_file(string nome_file){
    ifstream file(nome_file);
    if(!file.is_open()){
        cerr<<"errore nell'apertura del file"<<endl;
    }
    string riga;
    while(getline(file,riga)){
        cout<<riga<<endl;
    }
    file.close();
}

nodo get_nodo(){
    nodo ans;
    ans=ans.carattere;
    return ans;
}
/*
usare vector

vector<nodo> g1;
add_nodo(g1, "A");
stampa(g1);

add_nodo(G g, string etichetta){
    Nodo nuovo;
    nuovo.etichetta=etichetta;
    g.push(nuovo);
    g.push_back(nuovo);
}
*/