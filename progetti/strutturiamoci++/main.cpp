#include <iostream>
#include <ctime>
#include <stdlib.h>

using namespace std;
#define N 3
struct alunno{
    string nome;
    string cognome;
    int anno;
};

alunno getAlunno(int posizione, alunno v[]);
void riempi(alunno v[], int grandezza);
void stampa(alunno v[], int grandezza);

int main(){                                     //main
    srand(time(NULL));
    alunno vettore[N];
    int posizione=0;
    int grandezza =0;
    riempi(vettore,3);
    stampa(vettore,3);
    getAlunno(2,vettore);
    return 0;
}

void stampa(alunno v[], int grandezza){
    cout<<"======Alunni======"<<endl;
    for(int i=0;  i<grandezza; i++){
        cout<<"Nome: "<<v[i].nome<<endl;
        cout<<"Cognome: "<<v[i].cognome<<endl;
        cout<<"Anno: "<<v[i].anno <<endl;
    }
}

alunno getAlunno(int posizione, alunno v[]){
    alunno ans= v[posizione];
    return ans;
}

void riempi(alunno v[], int grandezza){
    string nome[3]={"Samuele", "Mimmo", "Marco"};
    string cognome[3]={"Rossi", "Verdi", "Bianchi"};
    int anno[3]={2007, 2006, 2005};
    int x=0;
    for(int i=0;i < grandezza;i++){
        x=rand()%3;
        v[i].anno=anno[x];
        x=rand()%3;
        v[i].nome=nome[x];
        x=rand()%3;
        v[i].cognome=cognome[x];
    }
}
