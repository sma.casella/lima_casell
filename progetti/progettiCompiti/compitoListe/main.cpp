#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

// Costanti
//define N_ANIMALI 12

// strutture
struct Animale {
  string nome;
  string razza;
};
struct ListaAnimali {
  Animale dato;
  ListaAnimali *next;
};

// Prototipi
void stampa(const Animale);
void stampa(const ListaAnimali *);
string get_nome(); // Restituisce un nome di animale a caso
string get_razza();  // Restituisce una razza a caso
Animale get_animale(); // Restituisce un animale con nome e raza presi a caso
ListaAnimali * get_lista_animali(int a, int b); // Restituisce il puntatore ad una lista di `N_ANIMALI` animali
int casualeTra(int a, int b);

int main() {
  srand((unsigned int)time(NULL));
  ListaAnimali *amici;
  int a, b;
  cout<<"Numero minimo di animali: ";
  cin >>a;
  cout<<endl<<"Numero massimo dei animali: ";
  cin>>b;
  cout<<endl;
  // Animale test;
  // cout << "Debug nome: " << get_nome() << "; numero di animali: " << N_ANIMALI <<endl;
  // cout << "Debug razza: " << get_razza() << "; numero di animali: " << N_ANIMALI <<endl;
  // test = get_animale();
  ///stampa(test);
  amici = get_lista_animali(a, b);
  stampa(amici);
  return 0;
}

// Implementazione
void stampa(const Animale a){
  //cout << "I'm `void stampa(Animale a)`: Lavori in corso" << endl;
  cout << "nome: " << a.nome << "; \t razza: " << a.razza << endl;
}
void stampa(const ListaAnimali * l){
  // cout << "I'm `void stampa(const ListaAnimali * l)`: Lavori in corso" << endl;
  if (l!=NULL) {
    stampa(l->dato);
    stampa(l->next); // ricorsione.
  } else {
    cout << "=== Fine lista animali ====" << endl;
  }
}
string get_nome(){
  string ans;
  string tmp[] = {"Bob", "Miao", "Bianchina", "Fido", "Fuffi"};
  int n = sizeof(tmp)/sizeof(tmp[0]);
  ans = tmp[rand()%n];
  return ans;
}
string get_razza(){
  string ans;
  string tmp[] = {"Cane", "Gatto", "Volatile", "Rettile"};
  int n = sizeof(tmp)/sizeof(tmp[0]);
  ans = tmp[rand()%n];
  return ans;
}
Animale get_animale(){
  Animale ans;
  ans.nome = get_nome();
  ans.razza = get_razza();
  return ans;
}
ListaAnimali * get_lista_animali(int a, int b){
  ListaAnimali *ans, *tmp;
  int n_animali = casualeTra(a, b); // Sarebbe da creare funzione `get_casuale_tra()` ...
  ans = new (ListaAnimali);
  ans->dato = get_animale();
  ans->next = NULL;
  tmp = ans;
  cout << "Creo una lista di " << n_animali << " animali." << endl; // TODO: vedere se togliere il messaggio.
  for(int i=1; i < n_animali; i++) {
    tmp->next = new(ListaAnimali);
    tmp = tmp->next;
    tmp->dato = get_animale();
    tmp->next = NULL;
  }
  return ans;
}
//aggiungo casuale tra
int casualeTra(int a, int b){
    return (rand()%(a-b-1)+b);
}
