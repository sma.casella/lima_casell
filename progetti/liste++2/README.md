# Informazioni sullo sviluppatore

Nome: Casella Matteo

Istituto: it Manzetti

Età: 16 anni

Mail: sma.casella@mail.scuole.vda.it

## Il programma di questo progetto

questo programma stampa a video una lista di alunni con voti.

ogni dato dei voti e dei alunni generato casualmente.

e per ogni alunno genera una domanda casuale che e' stata fatta all'alunno, con voto.

## Esempio di output del programma

- - - - - - - - - 

$ ./output
Classe di alunni con voti






======= Alunno =============
Nome: Mattia; Cognome: Verdi; Anno: 2007; Classe: 3cit
======== Media Voti: 6.3125 =============
        Materia: Italiano; Data: 19/7/2001; Voto: 8
        Materia: Italiano; Data: 18/8/2023; Voto: 6
        Materia: Storia; Data: 18/11/2022; Voto: 9.5
        Materia: Matematica; Data: 29/4/2004; Voto: 7
        Materia: Storia; Data: 29/6/2001; Voto: 6
        Materia: Matematica; Data: 13/6/2007; Voto: 6.5
        Materia: Storia; Data: 27/9/2002; Voto: 2
        Materia: Informatica; Data: 6/3/2005; Voto: 5.5
----------------------------
Una Domanda: Quando e' nato Giulio Cesare?; Voto: 2.5

======= Alunno =============
Nome: Mattia; Cognome: Neri; Anno: 1983; Classe: 3bcat
======== Media Voti: 6.5 =============
        Materia: Inglese; Data: 25/8/2019; Voto: 2
        Materia: Storia; Data: 9/11/2019; Voto: 7
        Materia: Italiano; Data: 12/10/2024; Voto: 10
        Materia: Informatica; Data: 4/10/2000; Voto: 7.5
        Materia: Informatica; Data: 4/3/2006; Voto: 10
        Materia: Storia; Data: 1/9/2008; Voto: 5.5
        Materia: Storia; Data: 18/11/2000; Voto: 6.5
        Materia: Inglese; Data: 30/11/2007; Voto: 8
        Materia: Storia; Data: 2/4/2006; Voto: 2
----------------------------
Una Domanda: Quando e' nato Giulio Cesare?; Voto: 5.5

======= Alunno =============
Nome: Valerio; Cognome: Pesci; Anno: 1989; Classe: 3cit
======== Media Voti: 6.3125 =============
        Materia: Italiano; Data: 25/3/2015; Voto: 4.5
        Materia: Matematica; Data: 14/7/2015; Voto: 5.5
        Materia: Inglese; Data: 1/10/2004; Voto: 3
        Materia: Storia; Data: 8/11/2006; Voto: 9.5
        Materia: Italiano; Data: 9/6/2008; Voto: 5.5
        Materia: Matematica; Data: 29/1/2000; Voto: 10
        Materia: Inglese; Data: 21/5/2017; Voto: 6
        Materia: Storia; Data: 13/9/2024; Voto: 6.5
----------------------------
Una Domanda: Come ti chiami?; Voto: 4.5

======= Alunno =============
Nome: Pietro; Cognome: Pesci; Anno: 1993; Classe: 3bcat
======== Media Voti: 4.83333 =============
        Materia: Informatica; Data: 23/3/2013; Voto: 3.5
        Materia: Italiano; Data: 17/4/2007; Voto: 4.5
        Materia: Informatica; Data: 2/9/2011; Voto: 5
        Materia: Inglese; Data: 29/9/2000; Voto: 5
        Materia: Informatica; Data: 14/11/2022; Voto: 5.5
        Materia: Inglese; Data: 22/2/2013; Voto: 5.5
----------------------------
Una Domanda: Il coccodrillo come fa?; Voto: 8

======= Alunno =============
Nome: Pietro; Cognome: Verdi; Anno: 1999; Classe: 1ait
======== Media Voti: 5.27778 =============
        Materia: Informatica; Data: 13/11/2010; Voto: 4.5
        Materia: Informatica; Data: 16/6/2008; Voto: 4.5
        Materia: Informatica; Data: 17/8/2006; Voto: 3.5
        Materia: Italiano; Data: 19/1/2014; Voto: 2.5
        Materia: Matematica; Data: 11/10/2022; Voto: 10
        Materia: Informatica; Data: 13/5/2012; Voto: 4.5
        Materia: Italiano; Data: 1/1/2018; Voto: 4
        Materia: Italiano; Data: 12/3/2011; Voto: 4.5
        Materia: Italiano; Data: 4/12/2012; Voto: 9.5
----------------------------
Una Domanda: Quanto e' il valore di pigreco?; Voto: 9

======= Alunno =============
Nome: Valerio; Cognome: Neri; Anno: 2001; Classe: 1ait
======== Media Voti: 6.08333 =============
        Materia: Storia; Data: 7/9/2004; Voto: 6.5
        Materia: Matematica; Data: 29/5/2000; Voto: 9
        Materia: Storia; Data: 4/1/2023; Voto: 2
        Materia: Storia; Data: 20/9/2021; Voto: 6.5
        Materia: Matematica; Data: 9/1/2021; Voto: 7
        Materia: Inglese; Data: 17/8/2006; Voto: 5.5
----------------------------
Una Domanda: Come ti chiami?; Voto: 3


- - - - - - - - -


