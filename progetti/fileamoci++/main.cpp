#include <iostream>
#include <fstream>
#include <string>

using namespace std;

void scambiaLettera(char& a, char& b){
    char temp=a;
    a=b;
    b=temp;
}

int main(){
    string nomeFile;
    cout<<"Scrivi il nome del file che vuoi leggere: ";
    cin>>nomeFile;

    if (nomeFile.length() < 2) {
        cout << "Il nome del file deve avere almeno 2 caratteri." << endl;
        return 1;
    }

    char primaLettera= nomeFile[0];
    char secondaLettera= nomeFile[1];

    //apro il file
    ifstream file(nomeFile);

    //verifica se il file � stato aperto
    if(file.is_open()){
        cout<<"File aperto correttamente! "<<endl<<endl;
        scambiaLettera(primaLettera, secondaLettera);
        char carattere;

        while(file.get(carattere)){
            cout<< carattere;
        }
        cout<<endl;

    //chiudo il file
    file.close();

    }else{
        cout<<"errore nell'apertura del file"<<endl;
    }


    return 0;
}
