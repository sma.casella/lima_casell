# Informazioni sullo sviluppatore

Nome: Casella Matteo

Istituto: it Manzetti

Età: 16 anni

Mail: sma.casella@mail.scuole.vda.it

## Il programma main di questo progetto

questo programma serve a stampare le tabelline della moltiplicazione
fino a un numero di colonne deciso dal programmatore e a un numero
di linee casuale

##Nota sull'utilizzo del programma

Va solo fino alla colonna della tabellina del 5

##Esempio di output del programma

- - - - - - - - - - - - - - -

 Stampa: 0| Stampa: 0| Stampa: 0| Stampa: 0| Stampa: 0| Stampa: 0|
 Stampa: 0| Stampa: 1| Stampa: 2| Stampa: 3| Stampa: 4| Stampa: 5|
 Stampa: 0| Stampa: 2| Stampa: 4| Stampa: 6| Stampa: 8| Stampa: 10|
 Stampa: 0| Stampa: 3| Stampa: 6| Stampa: 9| Stampa: 12| Stampa: 15|
 Stampa: 0| Stampa: 4| Stampa: 8| Stampa: 12| Stampa: 16| Stampa: 20|

-- - - - - -- - -  -- - - -- 
