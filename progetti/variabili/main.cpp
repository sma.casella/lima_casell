#include <iostream>
using namespace std;
int main() {
    int i;
    float f;
    double d;
    bool b;
    char c;
    cout << "Size of int: " << sizeof(i) << " bytes" <<endl;
    cout << "Size of float: " << sizeof(f) << " bytes" <<endl;
    cout << "Size of double: " << sizeof(d) << " bytes" <<endl;
    cout << "Size of bool: " << sizeof(b) << " bytes" <<endl;
    cout << "Size of char: " << sizeof(c) << " bytes" <<endl;
    return 0;
}