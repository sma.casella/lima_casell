# Mermaid e Grafi

per usare i grafi con meraid bisogna iniziare su un file markdown scrivendo
_":::mermaid"_ poi bisogna andare a capo e 
scrivwere"graph"+ la tipologia del grafo.
Dopodiche si usano le frecce come dimostrato nell'esempio
## Tipologie di grafi
graph TD--> grafico verticale

graph LR--> grafico orizzontale

## Esempio di grafo con descrizione G(V,E)
:::mermaid
graph LR
    A-->B;
    B-->A;
    B-->C;
    B-->D;
    C-->B;
    C-->D;
:::


G(V,E)

V={A,B,C,D}
E={(A,B);(B,A);(B,C);(B,D);(C,B);(C,D)}

## Alberi

Un albero e' un grafo non orientato
Dei esempi di alberi sono le directory, l'archivio git e i alberi genealogici

## Esempio di albero

:::mermaid
flowchart TD
    A --- B;
    A --- C;
    B --- C;
    B --- D;
    C --- E;
    C --- D;
    D --- A;
    D --- E;
    E --- B;
:::