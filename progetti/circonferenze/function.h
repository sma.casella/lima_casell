#ifndef FUNCTION_H_
#define FUNCTION_H_

#include <iostream>
#include <math.h>

using namespace std;

    double pi=3.14;
class Circonferenza{
public:
    Circonferenza(int x, int y, double raggio): x(x), y(y), raggio(raggio){
        this->x=x;
        this->y=y;
        this->raggio=raggio;
    }
    void getRaggio(Circonferenza circonferenza){
        raggio=circonferenza.raggio;
    }
    void stampa(Circonferenza circonferenza);
    double perimetro(Circonferenza circonferenza);
    double area(Circonferenza circonferenza);
private:
    int x,y;
    double raggio;
};

#endif // FUNCTION_H_
