#include "function.h"

bool esiste(string nf){
    bool ans=1;
    ifstream rfile;
    nf+=".html";
    rfile.open(nf);
    if(rfile.is_open()){
        cout<<"il file e' gia' esistente, cambiare nome file"<<endl;
        ans=true;
    }else{
        cout<<"il file non esiste, creo il file"<<endl;
        cout<<"."<<endl<<"."<<endl<<"."<<endl<<"il file e' stato creato"<<endl;
        ans=false;
    }
    rfile.close();
    return ans;
}

void creaFile(string nf){
    string nomeCodiceHtml =nf +".html";
    ofstream wfile(nomeCodiceHtml);
    wfile<<"<!DOCTYPE html>"<<endl;
    wfile<<"<html>"<<endl;
    wfile<<"<head>"<<endl;
    wfile<<"<title>Page Title</title>"<<endl;
    wfile<<"</head>"<<endl;
    wfile<<"<body>"<<endl<<endl;
    wfile<</*"/t"<<*/"<h1>Ciao Mondo!</h1>"<<endl;
    wfile<</*"/t"<<*/"<p>L'informatico non si arrende MAI!!</p>"<<endl<<endl;
    wfile<<"</body>"<<endl;
    wfile<<"</html>"<<endl;
    wfile.close();
}