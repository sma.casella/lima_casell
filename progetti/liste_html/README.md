# Informazioni sullo sviluppatore

Nome: Casella Matteo

Istituto: it Manzetti

Età: 2007

Mail: sma.casella@mail.scuole.vda.it

## Il programma di questo progetto

questo programma genera un codice html con delle tabelle,
i cui dati vengono riempiti dalle liste di Alunni con voti.

## Esempio di output del programma

- - - - - - - - - 

$ ./output.exe prova
Classe di alunni con voti
il file non esiste, creo il file
.
.
.
il file e' stato creato



- - - - - - - - -

## Esempio di programma generato

- - - - - - - - - - - -

<!DOCTYPE html>
<html>
<head>
<title>tabella con liste</title>
</head>


<body>
	<h1>ciao mondo!</h1>
	<p>questa e' una pagina html</p>

<table>
	<tr>
		<th>Alunno</th>
		<th>Anno</th>
		<th>Classe</th>
		<th>Voti</th>
		<th>Media Voti</th>
		<th>Domande</th>
	</tr>
	<tr>
		<td>NeriMarco</td>
		<td>1971</td>
		<td>2aafm</td>
		<td><table>
	<tr>
		<th>Materia</th>
		<th>Data</th>
		<th>Voto</th>
	</tr>
	<tr>
		<td>Informatica</td>
		<td>23/1/2014</td>
		<td>8.000000</td>
	</tr>
	<tr>
		<td>Inglese</td>
		<td>18/11/2007</td>
		<td>7.000000</td>
	</tr>
	<tr>
		<td>Informatica</td>
		<td>19/3/2009</td>
		<td>9.500000</td>
	</tr>
	<tr>
		<td>Informatica</td>
		<td>20/7/2015</td>
		<td>8.000000</td>
	</tr>
	<tr>
		<td>Informatica</td>
		<td>10/12/2016</td>
		<td>4.500000</td>
	</tr>
	<tr>
		<td>Italiano</td>
		<td>7/6/2017</td>
		<td>6.500000</td>
	</tr>
</table>
</td>
		<td>7.250000</td>
		<td><table>
	<tr>
		<th>Domanda</th>
		<th>Voto</th>
	<tr>
	<tr>
		<td>Quanti stati ha l'america?</td>
		<td>10.000000</td>
	</tr>
	<tr>
		<td>Quando e' nato Giulio Cesare?</td>
		<td>5.000000</td>
	</tr>
	<tr>
		<td>Quanti stati ha l'america?</td>
		<td>5.000000</td>
	</tr>
	<tr>
		<td>Quanto e' il valore di pigreco?</td>
		<td>4.500000</td>
	</tr>
	<tr>
		<td>Quando e' nato Giulio Cesare?</td>
		<td>6.000000</td>
	</tr>
	<tr>
		<td>Quanto e' il valore di pigreco?</td>
		<td>3.000000</td>
	</tr>
	<tr>
		<td>Di che colore e' il cavallo Bianco di Napoleone</td>
		<td>7.500000</td>
	</tr>
	<tr>
		<td>Quanti stati ha l'america?</td>
		<td>3.000000</td>
	</tr>
	<tr>
		<td>Come ti chiami?</td>
		<td>9.000000</td>
	</tr>
</table>
</td>
	</tr>
	<tr>
		<td>VerdiMarco</td>
		<td>1968</td>
		<td>4ait</td>
		<td><table>
	<tr>
		<th>Materia</th>
		<th>Data</th>
		<th>Voto</th>
	</tr>
	<tr>
		<td>Informatica</td>
		<td>6/8/2022</td>
		<td>9.000000</td>
	</tr>
	<tr>
		<td>Matematica</td>
		<td>30/4/2011</td>
		<td>9.000000</td>
	</tr>
	<tr>
		<td>Matematica</td>
		<td>8/5/2002</td>
		<td>8.500000</td>
	</tr>
	<tr>
		<td>Informatica</td>
		<td>11/10/2019</td>
		<td>3.000000</td>
	</tr>
	<tr>
		<td>Storia</td>
		<td>12/8/2015</td>
		<td>8.500000</td>
	</tr>
	<tr>
		<td>Storia</td>
		<td>21/6/2004</td>
		<td>6.000000</td>
	</tr>
	<tr>
		<td>Inglese</td>
		<td>16/7/2010</td>
		<td>4.000000</td>
	</tr>
	<tr>
		<td>Inglese</td>
		<td>2/3/2000</td>
		<td>6.000000</td>
	</tr>
	<tr>
		<td>Italiano</td>
		<td>18/4/2022</td>
		<td>7.500000</td>
	</tr>
</table>
</td>
		<td>6.833333</td>
		<td><table>
	<tr>
		<th>Domanda</th>
		<th>Voto</th>
	<tr>
	<tr>
		<td>Quanto e' il valore di pigreco?</td>
		<td>4.000000</td>
	</tr>
	<tr>
		<td>Quanto e' il valore di pigreco?</td>
		<td>2.000000</td>
	</tr>
	<tr>
		<td>Quanto e' il valore di pigreco?</td>
		<td>4.500000</td>
	</tr>
	<tr>
		<td>Come ti chiami?</td>
		<td>8.000000</td>
	</tr>
	<tr>
		<td>Quanto e' il valore di pigreco?</td>
		<td>4.000000</td>
	</tr>
	<tr>
		<td>Il coccodrillo come fa?</td>
		<td>3.500000</td>
	</tr>
	<tr>
		<td>Il coccodrillo come fa?</td>
		<td>2.500000</td>
	</tr>
</table>
</td>
	</tr>
</table>

</body>


</html>


- - - - - - - - - - - -

