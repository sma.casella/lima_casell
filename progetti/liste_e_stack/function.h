#ifndef FUNCTION_H
#define FUNCTION_H

#define N_Alunni 1
#include <iostream>
#include <cstring>
#include <ctime> 
#include <list>
using namespace std;

struct Alunno {
  string nome;
  string cognome;
  int anno;
  string classe;  
  struct ListaVoti * voti;
  double get_media();
  struct ListaDomande * domande;
};

struct ListaAlunni{
  Alunno dato;
  struct ListaAlunni * next;
};

struct Voto {
  string materia;
  string data;
  double numero;
};

struct ListaVoti {
  Voto dato;
  struct ListaVoti * next;
};

struct Domanda {
  string domanda;
  double voto;
  //struct ListaDomande * domande;
};

struct ListaDomande {
  Domanda dato;
  struct ListaDomande * next;
};

template <class T> void stampa(const T l) {
  if (l != NULL) {
    stampa(l->dato);
    stampa(l->next);
  } else {
    //cout << ">>>> Fine stampa con template <<<< " << endl;
  }
}

void saluta();
void stampa(Voto);
void stampa(Alunno);
void stampa(Domanda);
string get_nome();
string get_cognome();
string get_data();
string get_classe();
string get_domanda_fatta();
int get_anno();
double get_valore_voto();
string get_materia();
Alunno get_alunno();
Voto get_voto();
Domanda get_domanda();
ListaVoti * get_lista_voti();
ListaAlunni * get_lista_alunni();
ListaDomande * get_lista_domande();

int get_casuale_tra(int, int);
#endif
/*
    TODO: aggiustare il nom
*/