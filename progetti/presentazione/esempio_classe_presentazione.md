# Presentazione

---
# Strumenti per creare diagrammi

--

# Classe UML

:::mermaid
---
title: Circonferenza
---
classDiagram
    class Circonferenza {
        -raggio
        -double xc
        -double yc
        +void setXc()
        +void getXc()
        +void stampa()
    }
:::

--

Codice Utilizzato:

:::mermaid
---
title: Circonferenza
---
classDiagram
    class Circonferenza {
        -raggio
        -double xc
        -double yc
        +void setXc()
        +void getXc()
        +void stampa()
    }
:::

---
## esempi c++

--

### Esempio di classe

:::c++
... codice qui
...