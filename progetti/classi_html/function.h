#ifndef FUNCTION_H_
#define FUNCTION_H_

#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <fstream>

using namespace std;

class PaginaHtml{
public:
    PaginaHtml(string nome_file){
        this->head="";
        this->body="";
        this->title="titolo";
    };
    //string get_title();
    void make_pagina(string nf);
   /* void make_head();
    void make_body();
    string get_body();
    string get_head();*/
    string get_pagina();
    void add_head(string tag, string elemento, string tag_close);
    void add_body(string tag, string elemento, string tag_close);
private:
    string nome_file=" ";
    string title=" ";
    string body=" ";
    string head="";
};
int get_casuale(int, int);
bool esiste(string);
void leggi_file(string nf);

#endif