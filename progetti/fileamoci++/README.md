# Informazioni sullo sviluppatore

Nome: Casella Matteo

Istituto: it Manzetti

Età: 16 anni

Mail: sma.casella@mail.scuole.vda.it

## Il programma main di questo progetto

questo programma serve ad aprire un file(segnalando eventuali casi di errore nell'apertura),
eseguire la lettura carattere per carattere,
analizzarne il nome del file dato in linea di comando 
e scambiare fra loro i primi due caratteri del nome

## Esempio di output del programma
il programma al momento ha bisogno di essere corretto, quindi incollerò qui l'output di errore
- - - - - - - - - 

$ ./a.exe
Scrivi il nome del file che vuoi leggere: prova.txt
File aperto correttamente!

Il gatto
è un
animale domestico

- - - - - - - - -


