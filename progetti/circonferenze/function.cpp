#include "function.h"

Circonferenza::void stampa(Circonferenza circonferenza){
        cout<<"Punto x: "<<circonferenza.x<<endl<<"Punto y: "<<circonferenza.y<<endl<<"Raggio: "<<circonferenza.raggio<<endl;
    }

Circonferenza::double perimetro(Circonferenza circonferenza){
    double raggio;
    raggio=circonferenza.raggio;
    double diametro;
    diametro=raggio*2;
    double perimetro;
    perimetro=diametro*pi;
    return perimetro;
}

Circonferenza::double area(Circonferenza circonferenza){
    double raggio;
    raggio=circonferenza.raggio;
    double area;
    area=raggio^2*pi;
    return area;
}
