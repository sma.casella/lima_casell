#ifndef FUNCTION_H_
#define FUNCTION_H_

#include <iostream>
#include <fstream>
#include <vector> //TO DO: usare vector8
using namespace std;

/*class Nodo{
public:
    Nodo (char){
        this->carattere=' ';
    };
private:
    char carattere;
};*/
struct nodo{
    string carattere;
};
struct lista_nodi{
    nodo dato;
    struct lista_nodi * next;
};

string add_connessione(nodo, nodo);
void leggi_file(string nome_file);
bool esiste(string nome_file);
void crea_file(string nome_file);

nodo get_nodo();
void add_carattere();
#endif