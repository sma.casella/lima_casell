<map version="freeplane 1.12.1">
<!--To view this file, download free mind mapping software Freeplane from https://www.freeplane.org -->
<node TEXT="Informatica" FOLDED="false" ID="ID_696401721" CREATED="1610381621824" MODIFIED="1731322932257" STYLE="oval">
<font SIZE="18"/>
<hook NAME="MapStyle" zoom="1.332">
    <properties fit_to_viewport="false" show_tags="UNDER_NODES" edgeColorConfiguration="#808080ff,#ff0000ff,#0000ffff,#00ff00ff,#ff00ffff,#00ffffff,#7c0000ff,#00007cff,#007c00ff,#7c007cff,#007c7cff,#7c7c00ff" associatedTemplateLocation="template:/standard-1.6.mm"/>
    <tags category_separator="::"/>

<map_styles>
<stylenode LOCALIZED_TEXT="styles.root_node" STYLE="oval" UNIFORM_SHAPE="true" VGAP_QUANTITY="24 pt">
<font SIZE="24"/>
<stylenode LOCALIZED_TEXT="styles.predefined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="default" ID="ID_271890427" ICON_SIZE="12 pt" COLOR="#000000" STYLE="fork">
<arrowlink SHAPE="CUBIC_CURVE" COLOR="#000000" WIDTH="2" TRANSPARENCY="200" DASH="" FONT_SIZE="9" FONT_FAMILY="SansSerif" DESTINATION="ID_271890427" STARTARROW="NONE" ENDARROW="DEFAULT"/>
<font NAME="SansSerif" SIZE="10" BOLD="false" ITALIC="false"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.details"/>
<stylenode LOCALIZED_TEXT="defaultstyle.tags">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.attributes">
<font SIZE="9"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.note" COLOR="#000000" BACKGROUND_COLOR="#ffffff" TEXT_ALIGN="LEFT"/>
<stylenode LOCALIZED_TEXT="defaultstyle.floating">
<edge STYLE="hide_edge"/>
<cloud COLOR="#f0f0f0" SHAPE="ROUND_RECT"/>
</stylenode>
<stylenode LOCALIZED_TEXT="defaultstyle.selection" BACKGROUND_COLOR="#afd3f7" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#afd3f7"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.user-defined" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="styles.topic" COLOR="#18898b" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subtopic" COLOR="#cc3300" STYLE="fork">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.subsubtopic" COLOR="#669900">
<font NAME="Liberation Sans" SIZE="10" BOLD="true"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.important" ID="ID_67550811">
<icon BUILTIN="yes"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.flower" COLOR="#ffffff" BACKGROUND_COLOR="#255aba" STYLE="oval" TEXT_ALIGN="CENTER" BORDER_WIDTH_LIKE_EDGE="false" BORDER_WIDTH="22 pt" BORDER_COLOR_LIKE_EDGE="false" BORDER_COLOR="#f9d71c" BORDER_DASH_LIKE_EDGE="false" BORDER_DASH="CLOSE_DOTS" MAX_WIDTH="6 cm" MIN_WIDTH="3 cm"/>
</stylenode>
<stylenode LOCALIZED_TEXT="styles.AutomaticLayout" POSITION="bottom_or_right" STYLE="bubble">
<stylenode LOCALIZED_TEXT="AutomaticLayout.level.root" COLOR="#000000" STYLE="oval" SHAPE_HORIZONTAL_MARGIN="10 pt" SHAPE_VERTICAL_MARGIN="10 pt">
<font SIZE="18"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,1" COLOR="#0033ff">
<font SIZE="16"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,2" COLOR="#00b439">
<font SIZE="14"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,3" COLOR="#990000">
<font SIZE="12"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,4" COLOR="#111111">
<font SIZE="10"/>
</stylenode>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,5"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,6"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,7"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,8"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,9"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,10"/>
<stylenode LOCALIZED_TEXT="AutomaticLayout.level,11"/>
</stylenode>
</stylenode>
</map_styles>
</hook>
<hook NAME="AutomaticEdgeColor" COUNTER="4" RULE="ON_BRANCH_CREATION"/>
<node TEXT="Codici" POSITION="bottom_or_right" ID="ID_1449138952" CREATED="1731322484875" MODIFIED="1731322920758" HGAP_QUANTITY="9.5 pt" VSHIFT_QUANTITY="-25.5 pt">
<edge COLOR="#ff0000"/>
<node TEXT="c++" ID="ID_1082338776" CREATED="1731322710724" MODIFIED="1731322770781" VSHIFT_QUANTITY="-21 pt">
<node TEXT="classi" ID="ID_936266130" CREATED="1731322725442" MODIFIED="1731322774484" VSHIFT_QUANTITY="-11.25 pt">
<node TEXT="sovraccarico dei operatori(nei codici provare un sovraccarico alla volta)" ID="ID_371909400" CREATED="1731322739128" MODIFIED="1731323845797" VSHIFT_QUANTITY="-37.5 pt">
<node TEXT="operatore =" ID="ID_1341831285" CREATED="1731322780433" MODIFIED="1731322794072">
<node TEXT="Complesso&amp; operator=(const Complesso&amp; z){&#xa;            reale=z.reale;&#xa;            immag=z.immag;&#xa;            return *this;&#xa;        }" ID="ID_54990852" CREATED="1731322796056" MODIFIED="1731322815155"/>
</node>
<node TEXT="operatore +" ID="ID_214422698" CREATED="1731322829275" MODIFIED="1731322840197"/>
<node TEXT="operatore -" ID="ID_956618059" CREATED="1731322843462" MODIFIED="1731322848306"/>
<node TEXT="operatore *" ID="ID_654956371" CREATED="1731322850931" MODIFIED="1731322857008"/>
<node TEXT="operatore /" ID="ID_459922069" CREATED="1731322859477" MODIFIED="1731322865039"/>
</node>
</node>
<node TEXT="vettori" ID="ID_1703144757" CREATED="1731481601015" MODIFIED="1731482597273" VSHIFT_QUANTITY="-6.75 pt">
<node TEXT="/!\c&apos;è sempre bisogno di dichiarare la grandezza del vettore prima di dichiararlo. Non si deve fare dopo" ID="ID_1467741751" CREATED="1731481761375" MODIFIED="1731483089001" VSHIFT_QUANTITY="-17.25 pt">
<node TEXT="quando la grandezza del vettore viene settata come const, il puntatore crea una copia in memoria del vettore" ID="ID_522966916" CREATED="1731481922579" MODIFIED="1731482363230" HGAP_QUANTITY="13.25 pt" VSHIFT_QUANTITY="-12 pt"/>
</node>
<node TEXT="valore null" ID="ID_9462248" CREATED="1731481840058" MODIFIED="1731483099425">
<node TEXT="è un valore per il puntatore che viene riconosciuto facilmente dal computer" ID="ID_857855060" CREATED="1731483082798" MODIFIED="1731483209623" VSHIFT_QUANTITY="-9.75 pt"/>
</node>
<node TEXT="nodo" ID="ID_1136738662" CREATED="1731483217140" MODIFIED="1732088783992">
<node TEXT="quando si vuole creare un nuovo nodo per una lista si fa la riga" ID="ID_335841368" CREATED="1731483287072" MODIFIED="1731483405416" VSHIFT_QUANTITY="-10.5 pt">
<node TEXT="nodo *p=NULL;" ID="ID_307267255" CREATED="1731483348993" MODIFIED="1731483400809" VSHIFT_QUANTITY="-24.75 pt"/>
</node>
<node TEXT="dopo aver dichiarato il nuov nodo bisogna metterlo all&apos;interno della lista" ID="ID_1982291371" CREATED="1731483413618" MODIFIED="1731483655186"/>
<node TEXT="quando si fa un nodo bisogna creare la struttura però" ID="ID_1104214603" CREATED="1731483800620" MODIFIED="1731483908741" VSHIFT_QUANTITY="3.75 pt">
<node TEXT="struct nodo{ double n; struct nodo *prossimo; };" ID="ID_755281476" CREATED="1731483833494" MODIFIED="1732087565185" VSHIFT_QUANTITY="-5.25 pt">
<node TEXT="//il tipo di n dipende dal tipo di vettore di cui la struttura deve occuparsi" ID="ID_1166904613" CREATED="1732087566738" MODIFIED="1732087599679"/>
</node>
</node>
<node TEXT="link visti a lezione" ID="ID_1456070716" CREATED="1731483910897" MODIFIED="1731598617795">
<node TEXT="http://www.science.unitn.it/~brunato/labpro1/lista.html" ID="ID_257569846" CREATED="1731484248061" MODIFIED="1731598641188"/>
<node TEXT="http://didawiki.cli.di.unipi.it/lib/exe/fetch.php/informatica/prl/lect8-slides.pdf" ID="ID_1807563360" CREATED="1731927501692" MODIFIED="1731927504364"/>
</node>
</node>
</node>
<node TEXT="consigli" ID="ID_1322952730" CREATED="1731928115836" MODIFIED="1731928188677">
<node TEXT="compilazione" ID="ID_33774576" CREATED="1731928125587" MODIFIED="1731928198691" VSHIFT_QUANTITY="-46.5 pt">
<node TEXT="Se un codice ha uno warning, correggerlo anche se compila e funzziona correttamente" ID="ID_1864666741" CREATED="1731928139040" MODIFIED="1732088781534" HGAP_QUANTITY="36.5 pt" VSHIFT_QUANTITY="-63 pt"/>
</node>
</node>
</node>
</node>
<node TEXT="git" FOLDED="true" POSITION="bottom_or_right" ID="ID_101216430" CREATED="1731322936585" MODIFIED="1731322970004" HGAP_QUANTITY="27.5 pt" VSHIFT_QUANTITY="-15 pt">
<edge COLOR="#ff00ff"/>
<node TEXT="makefile" ID="ID_1660698981" CREATED="1731322956287" MODIFIED="1731322984268" HGAP_QUANTITY="29 pt" VSHIFT_QUANTITY="-18 pt">
<node TEXT="output: main.o function.o&#xa;&#x9;g++ main.o function.o -o output  -Wconversion -Wall&#xa;&#xa;main.o: main.cpp function.h&#xa;&#x9;g++ -c main.cpp  -Wconversion -Wall&#xa;&#xa;function.o: function.cpp function.h&#xa;&#x9;g++ -c function.cpp -Wconversion -Wall&#xa;&#xa;clean:&#xa;&#x9;rm -f *.o *.exe .~* *.gch output" ID="ID_1384360011" CREATED="1731322988018" MODIFIED="1731323013990" VSHIFT_QUANTITY="-29.25 pt"/>
</node>
<node TEXT="comandi" ID="ID_862920197" CREATED="1731323028795" MODIFIED="1731323202444">
<node TEXT="git clone" ID="ID_861732281" CREATED="1731323034784" MODIFIED="1731323202444" VSHIFT_QUANTITY="-24.75 pt">
<node TEXT="clona la repository con ssh" ID="ID_769376176" CREATED="1731323048765" MODIFIED="1731323196094" VSHIFT_QUANTITY="-21.75 pt"/>
</node>
<node TEXT="git add" ID="ID_365654216" CREATED="1731323209192" MODIFIED="1731323214396">
<node TEXT="-A aggiunge tutte le mod fatte nella repository" ID="ID_1623485502" CREATED="1731323218436" MODIFIED="1731323236273"/>
</node>
<node TEXT="git commit" ID="ID_377121023" CREATED="1731323242084" MODIFIED="1731323253071">
<node TEXT="-m &quot; &quot; permette di fare il commit delle modifiche fatte alla repository" ID="ID_266513287" CREATED="1731323255073" MODIFIED="1731323304407"/>
</node>
<node TEXT="git pull" ID="ID_1427964403" CREATED="1731323310822" MODIFIED="1731323316678">
<node TEXT="prende le modifiche fatte alla repository." ID="ID_303691074" CREATED="1731323319009" MODIFIED="1731323333334"/>
<node TEXT="fare sempre prima di fare git push, per verificare se ci sono problemi" ID="ID_1832829178" CREATED="1731323337297" MODIFIED="1731323361384"/>
<node TEXT="usare per prendere le modifiche fatte alla repository da un altro pc, così permettendo di modificare la repository da pc differenti" ID="ID_1272160724" CREATED="1731323365974" MODIFIED="1731323453032"/>
</node>
<node TEXT="git push" ID="ID_697892687" CREATED="1731323457660" MODIFIED="1731323461247">
<node TEXT="pubblica le modifiche fatte alla repository" ID="ID_648199028" CREATED="1731323464054" MODIFIED="1731323481857"/>
<node TEXT="fare sempre dopo un pull, così da verificare se funziona" ID="ID_1968082942" CREATED="1731323486181" MODIFIED="1731323570387"/>
<node TEXT="fare sempre il push nel pc alternativo, prima di fare il pull delle modifiche nell&apos;altro, sennò non funziona e si fanno casini" ID="ID_748041163" CREATED="1731323574781" MODIFIED="1731323667561"/>
</node>
<node TEXT="git status" ID="ID_144456963" CREATED="1731323677331" MODIFIED="1731323681485">
<node TEXT="dice in che stato si trova la repository e da consigli su come proseguire" ID="ID_1710168273" CREATED="1731323683384" MODIFIED="1731323707695"/>
</node>
<node TEXT="git diff" ID="ID_1701986685" CREATED="1731928564511" MODIFIED="1731928627256">
<node TEXT="fa vedere le differenze tra l&apos;ultimo commit e quello precedente" ID="ID_93077230" CREATED="1731928570308" MODIFIED="1731928628803" VSHIFT_QUANTITY="9 pt"/>
</node>
</node>
</node>
</node>
</map>
