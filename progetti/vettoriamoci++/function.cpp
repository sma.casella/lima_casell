#include "function.h"
#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

void riempiVettore(int v[], const int D){   //!riempi
    srand(time(NULL));
    for(int i=0; i<D; i++){
        v[i]=rand()%50+1;
    }
}
void stampaVettore(int v[], const int D){   //!stampa
    for (int i=0; i<D; i++){
        cout<<v[i]<<"\n";
    }
}
int minimo(int v[], const int D){           //!minimo
    int minimo=v[0];
    for (int i=1;i<D;i++){
        if(v[i]<minimo){
            minimo=v[i];
        }
    }
    return minimo;
}
int massimo(int v[], const int D){          //!massimo
    int massimo=v[0];
    for(int i=1;i<D;i++){
        if(v[i]>massimo){
            massimo=v[i];
        }
    }
    return massimo;
}
float media(int v[], const int D){          //!media
    float media;
    int somma=0;
    for(int i=0; i<D; i++){
        somma=somma+v[i];
    }
    media=somma/D;
    return media;
}

