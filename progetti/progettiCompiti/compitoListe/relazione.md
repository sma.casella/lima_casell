# Compito di informatica del _______


Autore: Picchiottino Roberto

## Consegna

Si vuole realizzare un programma che stampi a video il contenuto di una lista di animali domestici (nome e razza) uno per riga.

I nomi e le razze vengono generati in modo casuale. Il numero di elementi inseriti e stampati deve essere anch'esso casuale e compreso tra 1 a 12.

Qualcuno ha abbozzato una traccia del programma, tu hai il compito di terminarlo e di relazionarne il funzionamento.

Descrivi il comportamento iniziale del programma riportando l'output ottenuto e il comando utilizzato per compilare.

Completa il codice c++ del file main.cpp e crea una relazione sul suo funzionamento.

Il codice attualmente compila.

La directory si comporta come un archivio git (pull e push non sono configurati e non servono) pretanto puoi utilizzare
gli altri comandi git che conosci per controllare il lavoro fatto ma non dimenticarti di riportarli nella relazione,
utlizzali per migliorare il tuo lavoro di relazione.

Considera che il codice non � stato testato quindi potrebbe essere necessario effettuare modifiche anche sostanziali al programma.

Puoi decidere di rifare da zero il codice.

Inserisci il codice sorgente del `main.cpp` tra gli allegati.

## Svolgimento

### Se avessi svolto esercizio nel laboratorio

Dopo aver avviato il PC, inserito user e password della classe ho avviato la macchina virtuale con avvio da rete. Avviato il sistema LTSP da rete.

Mi sono collegato a LTSP con i dati indicati nella consegna senza problemi.
Nella directory home dell utente ho trovato:

```
[picchio@idea compito_liste]$ tree ../compito_liste/
../compito_liste/
    main.cpp
    makefile
    relazione.md

```

Decido di completare il programma al posto di crearne uno nuovo in quanto sembra impostato correttamente.


### Comportamento iniziale dell'applicazione

Strutture in uso:
```c++
struct Animale {
  string nome;
};

struct ListaAnimali {
  Animale dato;
  ListaAnimali *next;
};

```

Una serire di prototipi:

```c++
void stampa(Animale);
void stampa(const ListaAnimali *);
string get_nome(); // Restituisce un nome di animale a caso
string get_razza();  // Restituisce una razza a caso
Animale get_animale(); // Restituisce un animale con nome e raza presi a caso
ListaAnimali * get_lista_animali(); // Restituisce il puntatore ad una lista di `N_ANIMALI` animali
```

E nel `main()` il codice:

```c++
int main() {
  srand((unsigned int)time(NULL));
  ListaAnimali *amici;
  cout << "Debug: " << get_nome() << "; numero di animali: " << N_ANIMALI <<endl;
  amici = get_lista_animali();
  stampa(amici);
  return 0;
}
```

Eseguendo il codice deduco che la funzione `get_nome()` sembra funzionare in quanto restituisce il nome di un animale differente ad ogni esecuzione.

Per le funzioni:
```c++
amici = get_lista_animali();
stampa(amici);
```

Ci sar� del lavoro da fare in quanto non ancora implementate.

Osservando l'implementazione delle funzioni vedo che sono gi� utilizzabili ma vanno corrette o migliorate.

Inizio pertanto a lavorare per rispettare la consegna analizzando l'implementazione.


#### Compilazione

Per compilare il programma, avendo notato il `makefile` eseguo `make`.

```
[picchio@idea compito_liste]$ make
g++ main.cpp -Wall -Wconversion

```

#### Output attuale

Avendolo compilato senza problemi lo eseguo:

```
picchio@idea compito_liste]$ ./a.out 
Debug: Bob; numero di animali: 10
I'm `void stampa(const ListaAnimali * l)`: Lavori in corso
```

E si nota che non fornisce molti dei dati richiesti.

### Completare il codice

Inizio a lavorare e affiderò al `git log` le informazioni sulle modifiche effettuate.


#### Funzione `get_razza()`

E' stato sufficiente copiare il codice di `get_nome()` e adattarlo modificando il vettore `string tmp[]`.

Nel `main()` ho inserito:

```
cout << "Debug razza: " << get_razza() << "; numero di animali: " << N_ANIMALI <<endl;
```

Con il comando `git diff` osservo le modifiche effettuate (se fatto prima del commit):

```
@@ -26,7 +26,8 @@ ListaAnimali * get_lista_animali(); // Restituisce il puntatore ad una lista di
 int main() {
   srand((unsigned int)time(NULL));
   ListaAnimali *amici;
-  cout << "Debug: " << get_nome() << "; numero di animali: " << N_ANIMALI <<endl;
+  cout << "Debug nome: " << get_nome() << "; numero di animali: " << N_ANIMALI <<endl;
+  cout << "Debug razza: " << get_razza() << "; numero di animali: " << N_ANIMALI <<endl;
   amici = get_lista_animali();
   stampa(amici);
   return 0;
@@ -48,7 +49,11 @@ string get_nome(){
   return ans;
 }
 string get_razza(){
-  return "razza: Lavori in corso";
+  string ans;
+  string tmp[] = {"Cane", "Gatto", "Volatile", "Rettile"};
+  int n = sizeof(tmp)/sizeof(tmp[0]);
+  ans = tmp[rand()%n];;
+  return ans;
 }
 Animale get_animale(){
```

#### Altre modifiche

Per motivi di tempo, par capire le altre modifiche si faccia riferimento al sorgente allegato, riporterò solo eventuali problemi riscontrati.

- Gestione di un animale
- Stampa lista animali

```
$ git diff main.cpp
diff --git a/progetti/compito_liste/main.cpp b/progetti/compito_liste/main.cpp
index 7de983e..92d6b09 100644
--- a/progetti/compito_liste/main.cpp
+++ b/progetti/compito_liste/main.cpp
@@ -45,6 +45,12 @@ void stampa(Animale a){
 }
 void stampa(const ListaAnimali * l){
   cout << "I'm `void stampa(const ListaAnimali * l)`: Lavori in corso" << endl;
+  if (l!=NULL) {
+    stampa(l->dato);
+    stampa(l->next); // ricorsione.
+  } else {
+    cout << "=== Fine lista animali ====" << endl;
+  }
 }
 string get_nome(){
   string ans;

```

- Togli messaggi di debug inutili

```
[picchio@idea compito_liste]$ git diff main.cpp
diff --git a/progetti/compito_liste/main.cpp b/progetti/compito_liste/main.cpp
index 92d6b09..20e3c23 100644
--- a/progetti/compito_liste/main.cpp
+++ b/progetti/compito_liste/main.cpp
@@ -27,11 +27,11 @@ ListaAnimali * get_lista_animali(); // Restituisce il puntatore ad una lista di
 int main() {
   srand((unsigned int)time(NULL));
   ListaAnimali *amici;
-  Animale test;
-  cout << "Debug nome: " << get_nome() << "; numero di animali: " << N_ANIMALI <<endl;
-  cout << "Debug razza: " << get_razza() << "; numero di animali: " << N_ANIMALI <<endl;
-  test = get_animale();
-  stampa(test);
+  // Animale test;
+  // cout << "Debug nome: " << get_nome() << "; numero di animali: " << N_ANIMALI <<endl;
+  // cout << "Debug razza: " << get_razza() << "; numero di animali: " << N_ANIMALI <<endl;
+  // test = get_animale();
+  ///stampa(test);
   amici = get_lista_animali();
   stampa(amici);
   return 0;
@@ -44,7 +44,7 @@ void stampa(Animale a){
   cout << "nome: " << a.nome << "; razza: " << a.razza << endl;
 }
 void stampa(const ListaAnimali * l){
-  cout << "I'm `void stampa(const ListaAnimali * l)`: Lavori in corso" << endl;
+  // cout << "I'm `void stampa(const ListaAnimali * l)`: Lavori in corso" << endl;
   if (l!=NULL) {
     stampa(l->dato);
     stampa(l->next); // ricorsione.

```

- Fai in modo che gli animali siano tra 1 e 12

```
[picchio@idea compito_liste]$ git diff main.cpp
diff --git a/progetti/compito_liste/main.cpp b/progetti/compito_liste/main.cpp
index 05ff124..002fc7a 100644
--- a/progetti/compito_liste/main.cpp
+++ b/progetti/compito_liste/main.cpp
@@ -2,7 +2,7 @@
 
 using namespace std;
 // Costanti
-#define N_ANIMALI 10
+#define N_ANIMALI 12
 
 // strutture
 
@@ -74,11 +74,13 @@ Animale get_animale(){
 }
 ListaAnimali * get_lista_animali(){
   ListaAnimali *ans, *tmp;
+  int n_animali = rand()%N_ANIMALI + 1; // Sarebbe da creare funzione `get_casuale_tra()` ...
   ans = new (ListaAnimali);
   ans->dato = get_animale();
   ans->next = NULL;
   tmp = ans;
-  for(int i=1; i < N_ANIMALI; i++) {
+  cout << "Creo una lista di " << n_animali << " animali." << endl; // TODO: vedere se togliere il messaggio.
+  for(int i=1; i < n_animali; i++) {
     tmp->next = new(ListaAnimali);
     tmp = tmp->next;
     tmp->dato = get_animale();
```

- Rendi immodificabile il parametro di `void stampa(const Animale a)`

Al fine di non avere sorprese da una stampa che non deve modificare l'oggetto stampato.

### Comandi git

Ad ogni modifica importante creo una commit:

```
git add relazione.md
git commit -m "Crea indice della soluzione"
```

Con `git log` vedo lo storico:

```
commit cf91f7f5bc73ccbc8067334ed9297afa9e258cc4 (HEAD -> main)
Author: Picchiottino Roberto (idea) <r.picchiottino@mail.scuole.vda.it>
Date:   Sun Dec 8 10:11:26 2024 +0100

    Imposta numero animali casuale tra 1 e 12

commit c17136fc7b4cf0c4b0cb334a35df3316ab2c6e29
Author: Picchiottino Roberto (idea) <r.picchiottino@mail.scuole.vda.it>
Date:   Sun Dec 8 10:04:50 2024 +0100

    Migliora output

commit 457c89dbc051b5742eda06f694d9fd470f8a003b
Author: Picchiottino Roberto (idea) <r.picchiottino@mail.scuole.vda.it>
Date:   Sun Dec 8 10:01:46 2024 +0100

    Togli messaggi di debug inutili

commit 616ed50cb9605ef984bed6c4139af6fe8a2d10e8
Author: Picchiottino Roberto (idea) <r.picchiottino@mail.scuole.vda.it>
Date:   Sun Dec 8 09:58:58 2024 +0100

    Correggi funzione void stampa(const ListaAnimali * l)

commit eca33093b7de9c10966c202af05965498b6a8e5a
Author: Picchiottino Roberto (idea) <r.picchiottino@mail.scuole.vda.it>
Date:   Sun Dec 8 09:53:06 2024 +0100

    Aggiungo gestione anumale
    
    - Compila e funziona.

commit 737e64f391ea3cc4b88de171810fd7b787f1bcfb
Author: Picchiottino Roberto (idea) <r.picchiottino@mail.scuole.vda.it>
Date:   Sun Dec 8 09:48:33 2024 +0100

    Aggiungi razza ad animale

commit c39e7c5c976870f979f83a1900321687838eb276
Author: Picchiottino Roberto (idea) <r.picchiottino@mail.scuole.vda.it>
Date:   Sun Dec 8 09:40:57 2024 +0100

    Aggiungi codice funzione get_razza() e testalo

commit 0a8ea42e5d032729374835774654c344da2b4107
Author: Picchiottino Roberto (idea) <r.picchiottino@mail.scuole.vda.it>
Date:   Sun Dec 8 09:37:09 2024 +0100

    Analizza programma iniziale

commit 29a1a333956bbac76c5d8bfb646b4afd1bde9546
Author: Picchiottino Roberto (idea) <r.picchiottino@mail.scuole.vda.it>
Date:   Sun Dec 8 09:02:52 2024 +0100

    Comandi git e TODO: utili.

commit 2d7903f76d2f609fcf4842c2af70c788fb1f1cf2
Author: Picchiottino Roberto (idea) <r.picchiottino@mail.scuole.vda.it>
Date:   Sun Dec 8 09:00:10 2024 +0100

    Crea indice della soluzione

commit 962c5dc0aef28405f7c1f7e9c717aa397855cacc
Author: Picchiottino Roberto (idea) <r.picchiottino@mail.scuole.vda.it>
Date:   Thu Dec 5 14:17:46 2024 +0100

    Consegna compito liste dicembre 2024

```

## Stampa documento o hash

Il programma compila ed esegue, riporto output ottenuto in un paio di esecuzioni significative:

```
[picchio@idea compito_liste]$ make
make: «a.out» è aggiornato.
[picchio@idea compito_liste]$ ./a.out 
Creo una lista di 11 animali.
nome: Bob; 	 razza: Gatto
nome: Bob; 	 razza: Cane
nome: Bianchina; 	 razza: Rettile
nome: Miao; 	 razza: Volatile
nome: Fido; 	 razza: Gatto
nome: Bianchina; 	 razza: Cane
nome: Miao; 	 razza: Cane
nome: Fido; 	 razza: Volatile
nome: Miao; 	 razza: Volatile
nome: Miao; 	 razza: Rettile
nome: Fuffi; 	 razza: Volatile
=== Fine lista animali ====
[picchio@idea compito_liste]$ ./a.out 
Creo una lista di 1 animali.
nome: Bob; 	 razza: Volatile
=== Fine lista animali ====
```

Altre esecuzioni hanno creato liste di anumali comprese tra 1 e 12 come richiesto.


Per generare un pdf su utilizza `pandoc` ma al momento ci sono problemi con la codifica ed emacs.

```
[picchio@idea compito_liste]$ pandoc relazione.md -o relazione.md.pdf
[WARNING] relazione.md is not UTF-8 encoded: falling back to latin1.
```
Infatti il pdf generato non gestisce adeguatamente i caratteri accentati.

Viste le problematiche si crea il pdf da geany e si stampa il pdf con il codice md.

Il comando `geany relazione.md` apre il file con geany e dal menu stampa ho scelto formato di stampa pdf.

## Allegati

### Contenuto del `main.cpp`

Di seguito il contenuto del file `main.cpp`

```c++

#include <iostream>

using namespace std;
// Costanti
#define N_ANIMALI 12

// strutture

struct Animale {
  string nome;
  string razza;
};

struct ListaAnimali {
  Animale dato;
  ListaAnimali *next;
};

// Prototipi
void stampa(const Animale);
void stampa(const ListaAnimali *);
string get_nome(); // Restituisce un nome di animale a caso
string get_razza();  // Restituisce una razza a caso
Animale get_animale(); // Restituisce un animale con nome e raza presi a caso
ListaAnimali * get_lista_animali(); // Restituisce il puntatore ad una lista di `N_ANIMALI` animali

int main() {
  srand((unsigned int)time(NULL));
  ListaAnimali *amici;
  // Animale test;
  // cout << "Debug nome: " << get_nome() << "; numero di animali: " << N_ANIMALI <<endl;
  // cout << "Debug razza: " << get_razza() << "; numero di animali: " << N_ANIMALI <<endl;
  // test = get_animale();
  ///stampa(test);
  amici = get_lista_animali();
  stampa(amici);
  return 0;
}

// Implementazione

void stampa(const Animale a){
  //cout << "I'm `void stampa(Animale a)`: Lavori in corso" << endl;
  cout << "nome: " << a.nome << "; \t razza: " << a.razza << endl;
}
void stampa(const ListaAnimali * l){
  // cout << "I'm `void stampa(const ListaAnimali * l)`: Lavori in corso" << endl;
  if (l!=NULL) {
    stampa(l->dato);
    stampa(l->next); // ricorsione.
  } else {
    cout << "=== Fine lista animali ====" << endl;
  }
}
string get_nome(){
  string ans;
  string tmp[] = {"Bob", "Miao", "Bianchina", "Fido", "Fuffi"};
  int n = sizeof(tmp)/sizeof(tmp[0]);
  ans = tmp[rand()%n];;
  return ans;
}
string get_razza(){
  string ans;
  string tmp[] = {"Cane", "Gatto", "Volatile", "Rettile"};
  int n = sizeof(tmp)/sizeof(tmp[0]);
  ans = tmp[rand()%n];;
  return ans;
}
Animale get_animale(){
  Animale ans;
  ans.nome = get_nome();
  ans.razza = get_razza();
  return ans;
}
ListaAnimali * get_lista_animali(){
  ListaAnimali *ans, *tmp;
  int n_animali = rand()%N_ANIMALI + 1; // Sarebbe da creare funzione `get_casuale_tra()` ...
  ans = new (ListaAnimali);
  ans->dato = get_animale();
  ans->next = NULL;
  tmp = ans;
  cout << "Creo una lista di " << n_animali << " animali." << endl; // TODO: vedere se togliere il messaggio.
  for(int i=1; i < n_animali; i++) {
    tmp->next = new(ListaAnimali);
    tmp = tmp->next;
    tmp->dato = get_animale();
    tmp->next = NULL;
  }
  return ans;
}

```
