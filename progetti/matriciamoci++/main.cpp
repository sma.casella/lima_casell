#include <iostream>
#include <ctime>
#include <cstdlib>
#include <iomanip> //per la tabella

using namespace std;

void riempiMatrice(double** m, int r, int c){
    srand(time(NULL));
    for(int i=0; i<r; i++){
        for(int j=0; j<r; j++){
            m[i][j]= rand()%11;
        }
    }
}
/*
void stampaMatrice(double** m, int r, int c){
    for(int i=0; i<r; i++){
        for(int j=0; j<r; j++){
            cout<< m[i][j]<<"\t";
        }
    }

}
*/

void tabellaMatrice(double** m, int r, int c) {
    // Stampa la riga superiore della tabella
    for (int i = 0; i < c * 8 + 1; i++) {
        cout << "-";
    }
    cout << endl;

    for (int i = 0; i < r; i++) {
        cout << "|";
        for (int j = 0; j < c; j++) {
            cout << setw(7) << fixed << setprecision(0) << m[i][j] << "|";
        }
        cout << endl;

        // Stampa la linea divisoria tra le righe
        for (int i = 0; i < c * 8 + 1; i++) {
            cout << "-";
        }
        cout << endl;
    }
}

int main(){
    int righe=3;
    int colonne=4;
    double** matrice =new double*[righe];

    for(int i=0; i<righe; i++){
        matrice[i] =new double[colonne];
    }

    cout<<"Matrice: "<<endl;
    riempiMatrice(matrice, righe, colonne);
    //stampaMatrice(matrice, righe, colonne);
    tabellaMatrice(matrice, righe, colonne);

    for(int i=0; i<righe;i++){
        delete[] matrice[i];
    }
    delete[] matrice;

    return 0;
}
