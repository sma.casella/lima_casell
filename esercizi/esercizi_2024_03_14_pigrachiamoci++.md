Casella Matteo                  3^BIT                           16/05/2024
__________________________________________________________________________
# Esercizi di informatica assegnati il 16/05/2024
## Argomenti trattati
Pi greco Day. Video su pigreco song. Esercitazione su stima di pi greco
con metodo Monte Carlo. Spiegato metodo monte carlo e data traccia per soluzione
esercitazione pubblicata su archivio git della classe (lima).
## Consegna
Svolgere esercitazione su pigreco pubblicata su git e concentrarsi sul metodo
Monte Carlo, sulle fonti e sui collegamenti con eventi importanti della recente storia.
Riportare anche eventuali dubbi e gli estratti di codice c++ che
implementa la stima del valore di pi greco.
## Svolgimento
### Il 14 marzo viene detto il pi greco day. Anche il 22 luglio viene considerato il giorno di pigr. , come mai?
Il 14 marzo è detto pigreco day perché secondo la letteratura anglosassone delle date
il giorno viene scritto come 3/14, cioè le prime cifre del pigreco.
Invece il 22 luglio è pigreco day perché se dividi 22 per 7(luglio) fa 3.142857143
### Sapendo che l'area della circonferenza misura pigr. r2 e restringendo l'analisi ad ¼ di circonferenza, generare n punti casuali utilizzando la funzione drand48() che si inizializza con srand48()
 - - -- - - - - -- - - - - -- - - - - - -- - - - -- - - - - -- -
#include <stdlib>
void srand48(long int seedval);
double drand48(void);
int main(){
      x = drand48();
        y = drand48();
- - -- - -  -- - - - - - -- - - - -- - -  ---- --- -- -- -- - --
### Chiedi n long
- - - - - -- -  ---- -- -- -- -- -- - -- - - --- - - -- --- -- -
    long int n;
        double x, y,punti ;
        cout << "dammi punti da generare:" << endl;
        cin >> n;
- - -- --- -- - --- -- -- --- - - - -- -- - -- - -- -- --- -- -
### Genera n punti
- - - - - -- -  ---- -- -- -- -- -- - -- - - --- - - -- --- -- -
    for (int i = 0; i < n; i++) {
        x = drand48();
        y = drand48();
    }
- - - - - -- -  ---- -- -- -- -- -- - -- - - --- - - -- --- -- -
### Conta quanti sono all'interno della circonferenza (usa il teorema di Pitagora)
- - - - - -- -  ---- -- -- -- -- -- - -- - - --- - - -- --- -- -
    if ((x * x )+ (y * y) <= 1) {
        punti++;
    }
- - - - - -- -  ---- -- -- -- -- -- - -- - - --- - - -- --- -- -
### Esegui il rapporto tra lanci e “centri”stampa la stima di 
Rapporto = lanci / n
### Metodo monte Carlo
La simulazione Monte Carlo è un metodo utilizzato per quantificare il rischio associato
a un certo processo decisionale. Questa tecnica, basata sulla generazione di numeri casuali, è particolarmente utile
quando si hanno a disposizione molte variabili incognite e
quando non si dispone di dati storici o di esperienze passate per fare previsioni affidabili.
L’idea alla base della simulazione Monte Carlo è quella di creare una serie di scenari simulati,
ciascuno dei quali è caratterizzato da un insieme diverso di variabili.
Ogni scenario è determinato dalla generazione casuale di valori per ogni variabile.
Questo processo viene ripetuto molte volte, creando così un gran numero di scenari differenti.
## Fonti
14 marzo: https://www.vanityfair.it/article/perche-14-marzo-pi-greco-day#:~:text=Perch%C3%A9%20oggi%20%C3%A8%20il%20Pi%20Greco%20Day%3F&text=Per%20tutti%20quanti%20sono%20innamorati,della%20costante%20matematica%20pi%C3%B9%20famosa.
22 luglio: https://it.wikipedia.org/wiki/Giorno_del_pi_greco#:~:text=Il%20giorno%20dell'approssimazione%20di,fin%20dai%20tempi%20di%20Archimede.
metodo monte carlo: https://www.gironi.it/blog/il-metodo-montecarlo/
## Allegati
Md5-> 3aabccd9b83a6f436a9019b5fa171072
Questo esercizio-> git/lima_casell/esercizi/esercizi_2024_03_14_pigrechiamoci++.md
__________________________________________________________________
git/lima_casell/esercizi/esercizi_2024_03_14_pigrechiamoci++.md
