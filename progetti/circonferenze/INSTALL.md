# Installazione del programma
Per installare i file del programma bisogna andare sulla repository 
di git e installare i file.
## Compilazione del programma
per compilare i file del programma bisogna usare il seguente comando 
da linea di comando
- - - - - - -- - - - 
make
- - - - - - - - - - -
## Esecuzione del programma
per eseguire il programma usare il seguente comando
- - - - - - - - - - 
./output.exe
- - - - - - - - - - 
