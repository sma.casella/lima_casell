#include "function.h"
int get_casuale_tra (int min, int max) {
	int ans= min+rand()%(max-min+1);
	return ans;
  }
string PaginaHtml::get_pagina(){
	string ans;
	ans="<!DOCTYPE html>\n";
	ans+="<html>\n";
	ans+="\t<head>\n";
	ans+=this->head;
	ans+="\t</head>\n";
	ans+= "<link rel=\"stylesheet\" href=\"https://cdn.datatables.net/2.2.2/css/dataTables.dataTables.css\">\n";
	ans+= "<script src=\"https://code.jquery.com/jquery-3.7.1.js\"</script>\n";
	ans+= "<script src=\"https://cdn.datatables.net/2.2.2/js/dataTables.js\"></script>\n";
	ans+= "<body onload=\"table = new DataTable('#example');\">\n";
	ans+= "\t<table id=\"example\" class=\"display\" style=\"width:100%\">\n";
	ans+="\t<body>\n";
	ans+=this->body;
	ans+="\t</body>\n";
	ans+="</html>";
	return ans;
};
void PaginaHtml::make_pagina(string nf){
	string nome_codice= nf+".html";
	string file=get_pagina();
	ofstream wfile(nome_codice);
	wfile<<file;
	wfile.close();
};
bool esiste(string nf){
    bool ans=1;
    ifstream rfile;
    nf+=".html";
    rfile.open(nf);
    if(rfile.is_open()){
        cout<<"il file e' gia' esistente, lo leggo"<<endl;
        ans=true;
    }else{
        cout<<"il file non esiste, creo il file"<<endl;
        cout<<"."<<endl<<"."<<endl<<"."<<endl<<"il file e' stato creato"<<endl;
        ans=false;
    }
    rfile.close();
    return ans;
}

void PaginaHtml::add_head(string tag, string elemento, string tag_close){
	head+="\t<"+tag+">"+elemento+"</"+tag_close+">\n";
}

void PaginaHtml::add_body(string tag, string elemento, string tag_close){
	body+= "\t<"+tag+">"+elemento+"</"+tag_close+">\n";
}

void leggi_file(string nf){
  string nome_file= nf+".html";
  ifstream file(nome_file);

  if(!file.is_open()){
    cerr<<"Errore nell'apertura del file: "<< nome_file<<endl;
  }
  string riga;
  while(getline(file,riga)){
    cout<<riga<<endl;
  }

  file.close();
}
//Liste
string get_materia(){
	string ans = "placeholder";
	string tmp[]={"Matematica", "Informatica", "Italiano", "Storia", "Inglese"};
	ans=tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0])-1)];
	return ans;
  }
string get_data(){
	int giorno=rand()%30+1;
	int mese=rand()%12+1;
	if(mese==2 && giorno>28){
		giorno=rand()%27+1;
	}
	return to_string(giorno) + "/" + to_string(mese) + "/" + to_string(get_casuale_tra(2000, 2024));
}

string get_domanda_fatta() {
	string ans;
	ans =  "placeholder";
	string tmp[] = {"Come ti chiami?", "Quando e' nato Giulio Cesare?", "Quanto e' il valore di pigreco?", "Quanti stati ha l'america?", "Di che colore e' il cavallo Bianco di Napoleone", "Il coccodrillo come fa?"};
	ans = tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0])-1)];
	return ans;
}

string get_nome(){
	string ans;
	ans ="placeholder";
	string tmp[]={"Marco", "Emanuele", "Giuseppe", "samuele", "Mirco", "Roberto"};
	ans=tmp[get_casuale_tra(0,sizeof(tmp)/sizeof(tmp[0])-1)];
	return ans;
}
string get_cognome(){
	string ans;
	ans ="placeholder";
	string tmp[]={"Rossi","Verdi","Bianchi","costa","Pereira"};
	ans=tmp[get_casuale_tra(0,sizeof(tmp)/sizeof(tmp[0])-1)];
	return ans;
}
int get_eta(){
	int ans;
	ans=0;
	int tmp[]={14,15,16,17,18,19,40};
	int x=tmp[get_casuale_tra(0,sizeof(tmp)/sizeof(tmp[0])-1)];
	ans=x;
	return ans;
}
double get_numero(){
	double ans=0;
	double tmp[]={2,2.5,3,3.5,4,4.5,5,5.5,6,6.5,7,7.5,8,8.5,9,9.5,10};
	ans=tmp[get_casuale_tra(0,sizeof(tmp)/sizeof(tmp[0])-1)];
	return ans;
}
string get_classe(){
	string ans = "5ait default";
	string tmp[] = {"1ait", "2bit", "3cit", "4ait", "4bit", "4cafm", "3bcat", "2aafm"};
	ans = tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0])-1)];
	return ans;
}

Alunno get_alunno(){
	Alunno ans;
	ans.nome = get_nome();
	ans.cognome = get_cognome();
	ans.eta = get_eta();
	ans.classe = get_classe();
	ans.voti = get_voti();
	ans.domande=get_domande();
	return ans;
}
Domanda get_domanda(){
	Domanda ans;
	ans.domanda_fatta=get_domanda_fatta();
	ans.numero=get_numero();
	return ans;
}
Voto get_voto(){
	Voto ans;
	ans.data=get_data();
	ans.materia=get_materia();
	ans.numero=get_numero();
	return ans;
}

Lista_voti * get_voti() {
	Lista_voti * ans=NULL, *tmp;
	ans = new (Lista_voti);
	ans->dato = get_voto();
	ans->next = NULL;
	tmp = ans;
	for (int i = 0; i < get_casuale_tra(5, 10); i++) {
		tmp->next = new (Lista_voti);
		tmp = tmp->next;
		tmp->dato = get_voto();
		tmp->next = NULL;
	}
	return ans;
}

Lista_domande * get_domande() {
	Lista_domande * ans=NULL, *tmp;
	ans = new (Lista_domande);
	ans->dato = get_domanda();
	ans->next = NULL;
	tmp = ans;
	for (int i = 0; i < get_casuale_tra(5, 10); i++) {
		tmp->next = new (Lista_domande);
		tmp = tmp->next;
		tmp->dato = get_domanda();
		tmp->next = NULL;
	}
	return ans;
}

Lista_alunni * get_alunni() {
	Lista_alunni * ans=NULL, *tmp;
	ans = new (Lista_alunni);
	ans->dato = get_alunno();
	ans->next = NULL;
	tmp = ans;
	for (int i = 0; i < get_casuale_tra(5, 10); i++) {
		tmp->next = new (Lista_alunni);
		tmp = tmp->next;
		tmp->dato = get_alunno();
		tmp->next = NULL;
	}
	return ans;
}

void PaginaHtml::add_table() {
    table = "\t<table id=\"example\" class=\"display\" style=\"width:100%\">\n";
    table += "\t<tr>\n";
    table += "\t\t<th>Alunno</th>\n";
    table += "\t\t<th>Anno</th>\n";
    table += "\t\t<th>Classe</th>\n";
    table += "\t\t<th>Voti</th>\n";
    table += "\t\t<th>Domande</th>\n";
    table += "\t</tr>\n";

    Lista_alunni *classe = get_alunni();
    while (classe != NULL) {
        table += "\t<tr>\n";
        table += "\t\t<td>" + classe->dato.cognome + " " + classe->dato.nome + "</td>\n";
        table += "\t\t<td>" + to_string(classe->dato.eta) + "</td>\n";
        table += "\t\t<td>" + classe->dato.classe + "</td>\n";
        table += "\t\t<td>" + get_voti_html(classe->dato.voti) + "</td>\n";
        table += "\t\t<td>" + get_domande_html(classe->dato.domande) + "</td>\n";
        table += "\t</tr>\n";
        classe = classe->next;
    }
    table += "\t</table>\n";
}

string get_domande_html(Lista_domande *l) {
  string table = "<table>\n";
  table+="\t<tr>\n";
  table+="\t\t<th>Domanda</th>\n";
  table+="\t\t\t<th>Voto</th>\n";
  table+="\t<tr>\n";
  Lista_domande *tmp = l;
  while (tmp != NULL) {
    table+="\t<tr>\n";
    table+="\t\t\t<td>"+tmp->dato.domanda_fatta+"</td>\n";
    table+="\t\t<td>"+to_string(tmp->dato.numero)+"</td>\n";
    table+="\t</tr>\n";
    tmp = tmp->next;
  }

  table+= "</table>\n";
  return table;
}
string get_voti_html(Lista_voti *l){
  string table= "<table>\n";
  table+="\t<tr>\n";
  table+="\t\t<th>Materia</th>\n";
  table+="\t\t<th>Data</th>\n";
  table+="\t\t<th>Voto</th>\n";
  table+="\t</tr>\n";

  Lista_voti *tmp = l;
  while(tmp!= NULL){
    table+="\t<tr>\n";
    table+="\t\t<td>"+tmp->dato.materia+"</td>\n";
    table+="\t\t<td>"+tmp->dato.data+"</td>\n";
    table+="\t\t<td>"+to_string(tmp->dato.numero)+"</td>\n";
    table+="\t</tr>\n";
    tmp=tmp->next;
  }

  table+="</table>\n";
  return table;

}
void PaginaHtml::fill_body(){
	add_body("h1","Ciao Mondo!","h1");
	add_body("p","questa è una pagina html","p");
	add_table();
	body+=table;

}