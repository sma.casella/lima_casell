# Informazioni sullo sviluppatore

Nome: Casella Matteo

Istituto: it Manzetti

Anno: 2007

Mail: sma.casella@mail.scuole.vda.it

## Il programma di questo progetto

questo programma serve a stampare a video una lista di alunni, con ogni alunno che tiene una lista di voti.

Ogni singolo parametro e' generato casualmente

## Esempio di output del programma

- - - - - - - - - 

$ ./output.exe
Classe di alunni con voti









======= Alunno =============
Nome: Marco; Cognome: Neri; Anno: 2005; Classe: 4Ait
        Materia: Italiano; Data: 1/6/2003; Voto: 7
        Materia: Inglese; Data: 19/1/2007; Voto: 8.5
        Materia: Storia; Data: 26/6/2003; Voto: 4
        Materia: Matematica; Data: 11/6/2022; Voto: 7
        Materia: Storia; Data: 16/11/2000; Voto: 4
        Materia: Italiano; Data: 30/8/2003; Voto: 5
        Materia: Informatica; Data: 22/4/2016; Voto: 8.5

======= Alunno =============
Nome: Marco; Cognome: Verdi; Anno: 1964; Classe: 1Ait
        Materia: Matematica; Data: 4/5/2012; Voto: 6
        Materia: Inglese; Data: 14/1/2019; Voto: 6
        Materia: Italiano; Data: 15/10/2015; Voto: 10
        Materia: Italiano; Data: 4/7/2015; Voto: 3
        Materia: Storia; Data: 23/4/2009; Voto: 9.5
        Materia: Italiano; Data: 21/4/2018; Voto: 6.5
        Materia: Inglese; Data: 5/2/2003; Voto: 5
        Materia: Italiano; Data: 16/12/2014; Voto: 9.5

======= Alunno =============
Nome: Pietro; Cognome: Costa; Anno: 1982; Classe: 2Ait
        Materia: Matematica; Data: 2/1/2012; Voto: 10
        Materia: Matematica; Data: 21/8/2011; Voto: 5
        Materia: Matematica; Data: 17/8/2009; Voto: 8.5
        Materia: Italiano; Data: 12/3/2009; Voto: 9.5
        Materia: Inglese; Data: 4/2/2009; Voto: 3.5
        Materia: Italiano; Data: 5/10/2011; Voto: 7.5
        Materia: Inglese; Data: 6/6/2019; Voto: 4.5
        Materia: Storia; Data: 25/10/2023; Voto: 7

======= Alunno =============
Nome: Pietro; Cognome: Costa; Anno: 1992; Classe: 2Ait
        Materia: Informatica; Data: 4/1/2001; Voto: 8
        Materia: Storia; Data: 30/11/2002; Voto: 8
        Materia: Informatica; Data: 4/12/2003; Voto: 9
        Materia: Storia; Data: 23/7/2024; Voto: 4.5
        Materia: Italiano; Data: 6/3/2008; Voto: 3.5
        Materia: Italiano; Data: 30/1/2018; Voto: 7.5
        Materia: Italiano; Data: 16/11/2011; Voto: 4
        Materia: Storia; Data: 28/8/2016; Voto: 7

======= Alunno =============
Nome: Marco; Cognome: Neri; Anno: 1972; Classe: 4Ait
        Materia: Matematica; Data: 12/12/2000; Voto: 7
        Materia: Italiano; Data: 18/5/2012; Voto: 2
        Materia: Italiano; Data: 3/3/2016; Voto: 9.5
        Materia: Informatica; Data: 22/7/2024; Voto: 8
        Materia: Italiano; Data: 19/10/2000; Voto: 8.5
        Materia: Inglese; Data: 5/7/2007; Voto: 7

======= Alunno =============
Nome: Matteo; Cognome: Costa; Anno: 2014; Classe: 1Ait
        Materia: Informatica; Data: 1/9/2024; Voto: 3
        Materia: Matematica; Data: 4/1/2000; Voto: 6.5
        Materia: Italiano; Data: 20/9/2006; Voto: 6.5
        Materia: Storia; Data: 11/10/2016; Voto: 3.5
        Materia: Italiano; Data: 12/7/2009; Voto: 5
        Materia: Storia; Data: 10/8/2021; Voto: 2.5
        Materia: Informatica; Data: 29/6/2021; Voto: 3
        Materia: Italiano; Data: 6/9/2009; Voto: 4.5
        Materia: Inglese; Data: 24/6/2022; Voto: 6

======= Alunno =============
Nome: Matteo; Cognome: Costa; Anno: 1997; Classe: 4Ait
        Materia: Inglese; Data: 27/5/2009; Voto: 8
        Materia: Informatica; Data: 11/4/2009; Voto: 5.5
        Materia: Inglese; Data: 2/6/2010; Voto: 6
        Materia: Informatica; Data: 13/9/2010; Voto: 9.5
        Materia: Matematica; Data: 3/7/2002; Voto: 8
        Materia: Storia; Data: 19/9/2010; Voto: 4.5
        Materia: Inglese; Data: 27/11/2000; Voto: 4.5
        Materia: Matematica; Data: 15/3/2012; Voto: 7
        Materia: Inglese; Data: 28/6/2002; Voto: 4

======= Alunno =============
Nome: Valerio; Cognome: Costa; Anno: 1983; Classe: 2Ait
        Materia: Inglese; Data: 7/9/2005; Voto: 4
        Materia: Inglese; Data: 17/1/2003; Voto: 2.5
        Materia: Informatica; Data: 12/3/2000; Voto: 9.5
        Materia: Storia; Data: 30/9/2004; Voto: 2
        Materia: Inglese; Data: 7/10/2013; Voto: 6.5
        Materia: Informatica; Data: 24/9/2012; Voto: 7
        Materia: Inglese; Data: 30/9/2000; Voto: 8.5
        Materia: Storia; Data: 24/10/2021; Voto: 3

======= Alunno =============
Nome: Mattia; Cognome: Rosso; Anno: 1989; Classe: 2Ait
        Materia: Storia; Data: 7/2/2010; Voto: 10
        Materia: Storia; Data: 12/3/2008; Voto: 9
        Materia: Storia; Data: 13/6/2015; Voto: 6.5
        Materia: Informatica; Data: 20/8/2001; Voto: 6.5
        Materia: Matematica; Data: 16/11/2018; Voto: 9
        Materia: Storia; Data: 25/10/2013; Voto: 9.5
        Materia: Informatica; Data: 25/7/2016; Voto: 10
        Materia: Storia; Data: 11/1/2007; Voto: 2.5
        Materia: Storia; Data: 27/8/2006; Voto: 7

- - - - - - - - -


