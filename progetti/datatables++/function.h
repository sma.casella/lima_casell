#ifndef FUNCTION_H_
#define FUNCTION_H_

#include <iostream>
#include <string>
#include <ctime>
#include <cstdlib>
#include <fstream>

using namespace std;

class PaginaHtml{
public:
    PaginaHtml(string nome_file){
        this->head="";
        this->body="";
        this->title="titolo";
    };
    void make_pagina(string nf);
    string get_pagina();
    void add_head(string tag, string elemento, string tag_close);
    void add_body(string tag, string elemento, string tag_close);
    void add_table();
    void fill_body();
private:
    string nome_file=" ";
    string title=" ";
    string body=" ";
    string head="";
    string table="";

    bool enable_datatables=true;
};

struct Alunno{
    string nome;
    string cognome;
    int eta;
    string classe;
    struct Lista_voti * voti;
    struct Lista_domande* domande;
};
struct Voto{
    string materia;
    string data;
    double numero;
};
struct Domanda{
    string domanda_fatta;
    double numero;
};
struct Lista_alunni{
    Alunno dato;
    struct Lista_alunni* next;
};
struct Lista_voti{
    Voto dato;
    struct Lista_voti* next;
};
struct Lista_domande{
    Domanda dato;
    struct Lista_domande* next;

};
int get_casuale(int, int);
bool esiste(string);
void leggi_file(string nf);
//liste
string get_materia();
string get_data();
double get_numero();
string get_nome();
string get_cognome();
int get_eta();
string get_classe();
string get_domanda_fatta();
Alunno get_alunno();
Voto get_voto();
Domanda get_domanda();
Lista_alunni * get_alunni();
Lista_voti * get_voti();
Lista_domande * get_domande();

string get_voti_html(Lista_voti *l);
string get_domande_html(Lista_domande *l);
#endif