#include "function.h"

void saluta(){
    cout<<"Classe di alunni con voti "<<endl;
}

void stampa(Domanda d) {
  cout << "Domanda: " << d.domanda << "; Voto: " << d.voto << endl;
}

void stampa(Voto v) {
  cout << "\tMateria: " << v.materia << "; Data: " << v.data << "; Voto: " << v.numero << endl;
}

void stampa(Alunno a) {
  Domanda d;
  d=get_domanda();//togliere
  cout <<endl<< "======= Alunno =============" << endl;
  cout << "Nome: " << a.nome << "; Cognome: " << a.cognome << "; Anno: " << a.anno << "; Classe: " << a.classe << endl;
  cout << "======== Media Voti: " << a.get_media() << " =============" << endl;
  stampa(a.voti);
  cout<<endl<<"========= Domande ============== "<<endl;
  stampa(a.domande);
  //stampa(d);// creare una lista di domande per ogni voto
}           

string get_domanda_fatta() {
  string ans;
  ans =  "placeholder";
  string tmp[] = {"Come ti chiami?", "Quando e' nato Giulio Cesare?", "Quanto e' il valore di pigreco?", "Quanti stati ha l'america?", "Di che colore e' il cavallo Bianco di Napoleone", "Il coccodrillo come fa?"};
  ans = tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0])-1)];
  return ans;
}

Domanda get_domanda() {
  Domanda ans;
  ans.domanda = get_domanda_fatta();
  ans.voto = get_valore_voto();
  return ans;
}

double Alunno::get_media() {
  double ans, somma;
  int numero_voti;
  ans = 0;
  somma = 0;
  numero_voti = 0;
  ListaVoti *tmp = this->voti;
  if (tmp == NULL) {
    cout << "|!|Error : " << this->cognome << " non ha voti" << endl;
  } else {
    do {
      numero_voti++;
      somma += tmp->dato.numero;
      tmp = tmp->next;
    } while(tmp != NULL);
  }
  ans = somma/(double)numero_voti;
  return ans;
}

string get_nome() {
  string ans = "placeholder";
  string tmp[]={"Marco", "Mattia", "Matteo", "Valerio", "Pietro"};
  ans=tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0])-1)];
  return ans;
}
string get_cognome(){
  string ans = "placeholder";
  string tmp[]={"Rosso", "Pesci", "Costa", "Verdi", "Neri"};
  ans=tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0])-1)];
  return ans;
}

string get_data(){
  int giorno=rand()%30+1;
  int mese=rand()%12+1;
  if(mese==2 && giorno>28){
      giorno=rand()%27+1;
  }
  return to_string(giorno) + "/" + to_string(mese) + "/" + to_string(get_casuale_tra(2000, 2024));

}
double get_valore_voto(){
  double ans = ((double)get_casuale_tra(4, 20))/2.;
  return ans;
}

string get_classe(){
  string ans = "5ait default";
  string tmp[] = {"1ait", "2bit", "3cit", "4ait", "4bit", "4cafm", "3bcat", "2aafm"};
  ans = tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0])-1)];
  return ans;
}

string get_materia(){
  string ans = "placeholder";
  string tmp[]={"Matematica", "Informatica", "Italiano", "Storia", "Inglese"};
  ans=tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0])-1)];
  return ans;
}

int get_anno() {
  int ans=get_casuale_tra(1960, 2020);
  return ans;
}

Alunno get_alunno() {
  Alunno ans;
  ans.nome = get_nome();
  ans.cognome = get_cognome();
  ans.anno = get_anno();
  ans.classe = get_classe();
  ans.voti = get_lista_voti();
  ans.domande=get_lista_domande();
  return ans;
}

ListaVoti * get_lista_voti() {
  ListaVoti * ans=NULL, *tmp;
  ans = new (ListaVoti);
  ans->dato = get_voto();
  ans->next = NULL;
  tmp = ans;
  for (int i = 0; i < get_casuale_tra(5, 10); i++) {
    tmp->next = new (ListaVoti);
    tmp = tmp->next;
    tmp->dato = get_voto();
    tmp->next = NULL;
  }
  return ans;
}

ListaAlunni * get_lista_alunni() {
  ListaAlunni *ans = NULL, *tmp;
  ans = new (ListaAlunni);
  ans->dato = get_alunno();
  ans->next = NULL;
  tmp = ans;
  for (int i = 0; i < N_Alunni; i++) {
    tmp->next = new (ListaAlunni);
    tmp = tmp->next;
    tmp->dato = get_alunno();
    tmp->next = NULL;
    cout<<endl;
  }
  return ans;
}

ListaDomande * get_lista_domande() {
  ListaDomande * ans=NULL, *tmp;
  ans = new (ListaDomande);
  ans->dato = get_domanda();
  ans->next = NULL;
  tmp = ans;
  for (int i = 0; i < get_casuale_tra(5, 10); i++) {
    tmp->next = new (ListaDomande);
    tmp = tmp->next;
    tmp->dato = get_domanda();
    tmp->next = NULL;
  }
  return ans;
}

Voto get_voto() {
  Voto ans;
  ans.materia = get_materia();
  ans.data = get_data();
  ans.numero = get_valore_voto();
  //ans.domande= get_lista_domande();
  return ans;
}

int get_casuale_tra(int a, int b) {
  return (rand()%(b-a+1)+a);
}
