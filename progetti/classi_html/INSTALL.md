# installazione del programma del progetto base

per installare il programma main.cpp cliccare sull'icona di download 
nella repository.

## Compilazione del programma main

per compilare i file del programma bisogna eseguire il seguente comando da linea di comando

- - - - - - - - - -

make

- - - - - - - - - -

## Esecuzione del programma main

invece per eseguire il programma usare il seguente comando da linea di comando e aggiungere al posto dei puntini il nome del file SENZA estensione
- - - - - - - - - -

./output.exe .....

- - - - - - - - - -

## comando clean

con il seguente comando si eliminano i file extra, cioè i 
.o e output che vengono creati con la compilazione dei file c++
- - - - - - - - - -

make clean

- - - - - - - - - -