#include <iostream>
#include <string>
#include <fstream>
#include <ctime>
#include <cstdlib>

using namespace std;

struct persona{
    string nome;
    string cognome;
    int annoNascita;
};

void stampaAutore(persona a){
    cout<<"Autore del programma: "<<endl<<"Nome e Cognome-> "<<a.nome<<" "<<a.cognome<<endl<<"Anno di nascita-> "<<a.annoNascita<<endl;
}

void stampaVettore(persona v[], int D){
    cout<<"=====Stampa Vetore====="<<endl;
    for(int i=0;i<D;i++){
        cout<<"=====Persona "<<i+1<<"====="<<endl;
        cout<<"Nome: "<<v[i].nome<<endl;
        cout<<"Cognome: "<<v[i].cognome<<endl;
        cout<<"Anno: "<<v[i].annoNascita<<endl;
    }

}
void riempiVettore(persona v[], int D){
    string nome[3]={"Mario", "Luigi", "Giorgio"};
    string cognome[3]={"Rossi", "Verdi", "Bianchi"};
    int annoNascita[3]={2004, 2007, 2001};
    int x=0;
    for(int i=0; i<D; i++){
        x=rand()%3;
        cout<<v[x].nome<<endl;
        x=rand()%3;
        cout<<v[x].cognome<<endl;
        x=rand()%3;
        cout<<v[x].annoNascita<<endl;
    }
}

void ordinaVettore(persona v[], int D){
    persona temp;
    for(int i=0;i<D;i++){
        for(int j=i+1;j<D;j++){
            if(v[j].annoNascita>v[i].annoNascita){
                temp=v[j];
                v[j]=v[i];
                v[i]=temp;
            }
        }
    }
}

int main(){
    srand((unsigned int)time(NULL));
    int DIM=5;
    persona vettore[DIM];
    persona autore;
    string nomeFile="vettorePersone.csv";

    autore.nome="Matteo";
    autore.cognome="Casella";
    autore.annoNascita=2007;

    riempiVettore(vettore, DIM);
    stampaVettore(vettore,DIM);
    cout<<endl<<"Seconda stampa "<<endl;
    ordinaVettore(vettore, DIM);
    stampaVettore(vettore,DIM);     //!TODO sistemare l'output di questo codice, poich� non stampa per bene

    //stampaAutore(autore);

    return 0;
}
