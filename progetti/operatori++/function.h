#ifndef FUNCTION_H_
    #define FUNCTION_H_

    #include <iostream>
    #include <cmath>

    using namespace std;

    class Complesso{
    public:
        Complesso(double r=0.0, double i=0.0):reale(r),immag(i){}

        ~Complesso(){
            cout<<"Il programma e' finito"<<endl;
        }

        void stampa();

        void setComplesso(double r, double i){
            reale=r;
            immag=i;
        }

        double getImmag(){
            return immag;
        }

        double getReale(){
            return reale;
        }

        double modulo();

        double fase();

        void stampaPolari();

        Complesso& operator=(const Complesso& z){
            reale=z.reale;
            immag=z.immag;
            return *this;
        }
      
        Complesso& operator+ (const Complesso& y, const Complesso& z){
            Complesso complesso= (y.reale+z.reale,y.immag+z.immag);
            return complesso;
        }
/*
        friend Complesso& operator- (const Complesso& y, const Complesso& z){
            Complesso complesso=(y.reale-z.reale,y.immag-z.immag);
            return complesso ;
        }

        friend Complesso& operator* (const Complesso& y, const Complesso& z){
            Complesso complesso(y.reale*z.reale,y.immag*z.immag);
            return complesso ;
        }

        friend Complesso& operator/ (const Complesso& y, const Complesso& z){
            Complesso complesso= (y.reale/z.reale, y.immag/z.immag);
            return complesso;
        }

      */
        
    private:
        double reale, immag;
        double mod, fas;
    };

#endif // FUNCTION_H_
