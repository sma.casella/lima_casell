#include "function.h"
//!qua main
int main(){
    Counter c1(10);
    Counter *c2 = new Counter(20);

    cout<<c1.getValue()<<endl;
    c1.increment(5);
    cout<<c1.getValue()<<endl;
    cout<<c2->getValue()<<endl;
    c2->increment(5);
    cout<<c2->getValue()<<endl;
    delete c2;


    Complex a(1,4), b(3,8), c;
    a.print();
    b.print();
    c=a.add(a,b);
    c.print();
    Complex d(c);
    d=d.multiply(c,d);
    d.print();

    return 0;
}
