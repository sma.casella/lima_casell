#ifndef FUNCTION_H_
#define FUNCTION_H_

#include <iostream>
#include <cstring>
#include <ctime>
#include <cstdlib>
#include <list>

using namespace std;

struct Alunno{
    string nome;
    string cognome;
    int anno;
    string classe;
    struct ListaVoti * voti;
};

struct ListaAlunni{
    Alunno dato;
    struct ListaAlunni * next;
};
struct Voto{
  string materia;
  string data;
  double numero;
  Voto* next;  
};
struct ListaVoti{
    Voto dato;
    struct ListaVoti * next;
};


struct Domanda{
    string domanda;
    double voto;
    struct ListaDomande * domande;
};

struct ListaDomande{
    Domanda dato;
    struct ListaDomande * next;
};

list<ListaDomande> lista;

void saluta();
void stampa(Alunno a);
void stampa(Voto v);
void stampa(Domanda d);
void stampa(const ListaVoti * l);
void stampa(const ListaAlunni * l);
void stampa(ListaDomande );
string getNome();
string getCognome();
string classe();
string getData();
string getMateria();
int getAnno();
double getNumero();
Alunno getAlunno();
Voto getVoto();
string getDomandaFatta();
Domanda getDomanda();
ListaVoti * getListaVoti();
ListaAlunni * getListaAlunni();
ListaDomande * getListaDomande();
int casualeTra(int a, int b);


#endif // FUNCTION_H_