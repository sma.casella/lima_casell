#include "function.h"

void saluta(){
    cout<<"Classe di alunni con voti "<<endl;
}

void stampa(Voto v) {
  cout << "\tMateria: " << v.materia << "; Data: " << v.data << "; Voto: " << v.numero << endl;
}

void stampa(Alunno a) {
  cout <<endl<< "======= Alunno =============" << endl;
  cout << "Nome: " << a.nome << "; Cognome: " << a.cognome << "; Anno: " << a.anno << "; Classe: " << a.classe << endl;
  stampa(a.voti);
}

string get_nome() {
  string ans = "placeholder";
  string tmp[]={"Marco", "Mattia", "Matteo", "Valerio", "Pietro"};
  ans=tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0])-1)];
  return ans;
}
string get_cognome(){
  string ans = "placeholder";
  string tmp[]={"Rosso", "Pesci", "Costa", "Verdi", "Neri"};
  ans=tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0])-1)];
  return ans;
}

string get_data(){
  int giorno=rand()%30+1;
  int mese=rand()%12+1;
  if(mese==2 && giorno>28){
      giorno=rand()%27+1;
  }
  return to_string(giorno) + "/" + to_string(mese) + "/" + to_string(get_casuale_tra(2000, 2024));

}
double get_valore_voto(){
  double ans = ((double)get_casuale_tra(4, 20))/2.;
  return ans;
}

string get_classe(){
  string ans = "placeholder";
  string tmp[] = {"1Ait", "2Ait", "3Ait", "4Ait", "5Ait"};
  ans = tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0])-1)];
  return ans;
}

string get_materia(){
  string ans = "placeholder";
  string tmp[]={"Matematica", "Informatica", "Italiano", "Storia", "Inglese"};
  ans=tmp[get_casuale_tra(0, sizeof(tmp)/sizeof(tmp[0])-1)];
  return ans;
}

int get_anno() {
  int ans=get_casuale_tra(1960, 2020);
  return ans;
}

Alunno get_alunno() {
  Alunno ans;
  ans.nome = get_nome();
  ans.cognome = get_cognome();
  ans.anno = get_anno();
  ans.classe = get_classe();
  ans.voti = get_lista_voti();
  return ans;
}

ListaVoti * get_lista_voti() {
  ListaVoti * ans=NULL, *tmp;
  ans = new (ListaVoti);
  ans->dato = get_voto();
  ans->next = NULL;
  tmp = ans;
  for (int i = 0; i < get_casuale_tra(5, 10); i++) {
    tmp->next = new (ListaVoti);
    tmp = tmp->next;
    tmp->dato = get_voto();
    tmp->next = NULL;
  }
  return ans;
}

ListaAlunni * get_lista_alunni() {
  ListaAlunni *ans = NULL, *tmp;
  ans = new (ListaAlunni);
  ans->dato = get_alunno();
  ans->next = NULL;
  tmp = ans;
  for (int i = 0; i < get_casuale_tra(5, 10); i++) {
    tmp->next = new (ListaAlunni);
    tmp = tmp->next;
    tmp->dato = get_alunno();
    tmp->next = NULL;
    cout<<endl;
  }
  return ans;
}

Voto get_voto() {
  Voto ans;
  ans.materia = get_materia();
  ans.data = get_data();
  ans.numero = get_valore_voto();
  return ans;
}

int get_casuale_tra(int a, int b) {
  return (rand()%(b-a+1)+a);
}
