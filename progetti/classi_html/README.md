# Informazioni sullo sviluppatore

Nome: Casella Matteo

Istituto: it Manzetti

Anno: 2007

Mail: sma.casella@mail.scuole.vda.it

## Il programma di questo progetto

questo programma crea un file html usando classi c++

## Esempio di output del programma

- - - - - - - - - 
$ ./output.exe main
il file non esiste, creo il file
.
.
.
il file e' stato creato

- - - - - - - - -

## Esempio di programma che viene generato

- - - - - - - - - - -

<!DOCTYPE html>
<html>
<head>
<title>fake git</title>
</head>
<h1>ciao mondo!</h1>
<p>L'informatico non si e' ancora arreso</p>
</html>

- - - - - - - - - - -