#include "function.h"

int main(){
    Complesso a(1,4);
    a.stampa();
    cout<<"La parte reale e': "<<a.getReale()<<endl;
    cout<<"La parte immaginaria e': "<<a.getImmag()<<endl;

    cout<<"Il modulo e': "<<a.modulo()<<endl;
    cout<<"La fase e': "<<a.fase()<<endl;
    cout<<"Coordinate polari: ";
    a.stampaPolari();
    cout<<endl;
    return 0;
}
