#ifndef FUNCTION_H
#define FUNCTION_H

#include <iostream>
#include <cstring>
#include <ctime> 

using namespace std;

struct Alunno {
  string nome;
  string cognome;
  int anno;
  string classe;  
  struct ListaVoti * voti;
};

struct ListaAlunni{
  Alunno dato;
  struct ListaAlunni * next;
};

struct Voto {
  string materia;
  string data;
  double numero;
};

struct ListaVoti {
  Voto dato;
  struct ListaVoti * next;
};


template <class T> void stampa(const T l) {
  if (l != NULL) {
    stampa(l->dato);
    stampa(l->next);
  } else {
    //cout << ">>>> Fine stampa con template <<<< " << endl;
  }
}

void saluta();
void stampa(Voto);
void stampa(Alunno);
string get_nome();
string get_cognome();
string get_data();
string get_classe();
int get_anno();
double get_valore_voto();
string get_materia();
Alunno get_alunno();
Voto get_voto();
ListaVoti * get_lista_voti();
ListaAlunni * get_lista_alunni();

int get_casuale_tra(int, int);
#endif
