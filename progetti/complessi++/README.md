# Il programmatore
Cognome: Casella
Nome: Matteo
età: 17
istituto: Manzetti
mail: sma.casella@mail.scuole.vda.it

## Il programma
Il programma usa la classe dei numeri complessi per:
-crare un numero complesso
-Stampare il numero complesso
-Stampare la sua parte reale
-Stampare la sua parte immaginaria
-Calcolare e stampare la fase
-Calcolare e stampare il modulo

### Esempio di esecuzione del programma

- - - - - - - - - - -

$ ./output
(1,4)
La parte reale e': 1
La parte immaginaria e': 4
Il modulo e': 4.12311
La fase e': 1.32582
Il programma e' finito

- - - - - - - - - - -

## flowchart
All'interno del progetto c'e' il file flowchart.md, che rappresenta l'intero
programma in un flowchart mermaid
