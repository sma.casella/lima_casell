:::mermaid
flowchart TD;
    Start-->NumeroComplesso;
    NumeroComplesso-->StampaComplesso;
    NumeroComplesso-->ParteReale;
    NumeroComplesso-->ParteImmaginaria;
    NumeroComplesso-->Modulo;
    NumeroComplesso-->Fase;
    StampaComplesso-->End;
    ParteReale-->End;
    ParteImmaginaria-->End;
    Modulo-->End;
    Fase-->End;
:::
