#include "function.h"
#include <iostream>

using namespace std;

void Complesso::stampa(){
    cout<<"("<<reale<<","<<immag<<")"<<endl;
}

double Complesso::fase(){
    fas=atan(immag/reale);
    return fas;
}

double Complesso::modulo(){
    mod=sqrt(pow(reale,2)+pow(immag,2));
    return mod;
}

void Complesso::stampaPolari(){
    cout<<"("<<mod<<" ,"<<fas<<")"<<endl;
}