#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>

using namespace std;

struct appartamento{
    int nStanze;
    int metriQuadri;
    int nBagni;
    float prezzo;
    string comune;
    int box;
};

void riempi(appartamento &a);
void stampa(appartamento a);
void riempiVettore(appartamento V[], int D);
void stampaVettore(appartamento V[], int D);
void ordinaPerPrezzo(appartamento V[], int D);

int main(){
    int DIM=3;
    appartamento vettore[DIM];
    appartamento app;

/*    riempi(app);
    stampa(app);
*/
    riempiVettore(vettore, DIM);
    //stampaVettore(vettore, DIM);
    ordinaPerPrezzo(vettore, DIM);
    cout<<endl<<"====Appartamenti ordinati per prezzo===="<<endl;
    stampaVettore(vettore,DIM);

    return 0;
}

void riempi(appartamento &a){
    cout<<"Inserire il numero di stanze-> ";
    cin >> a.nStanze;

    cout<<"Inserire i metri Quadri-> ";
    cin >> a.metriQuadri;

    cout<<"Inserire il numero di bagni-> ";
    cin >> a.nBagni;

    cout<<"Inserire il prezzo-> ";
    cin >> a.prezzo;

    cin.ignore();
    cout << "Inserire il comune-> ";
    getline(cin, a.comune);

    cout<<"Inserire numero di box-> ";
    cin >> a.box;
}

void stampa(appartamento a){    //!TO DO sistemare l'output delle procedure
    cout<<"=====Appartamento====="<<endl;
    cout<<"Numero di stanze: "<<a.nStanze<<endl;
    cout<<"Numero di metri quadri: "<<a.metriQuadri<<endl;
    cout<<"Numero di bagni: "<<a.nBagni<<endl;
    cout<<"Prezzo: "<<a.prezzo<<endl;
    cout<<"Comune: "<<a.comune<<endl;
    cout<<"Numero di box: "<<a.box<<endl;
}

void riempiVettore(appartamento V[], int D){
    cout<<"=====Riempi il vettore====="<<endl;
    for(int i=0; i<D; i++){
        cout<<"Inserire il numero di stanze-> ";
        cin >> V[i].nStanze;

        cout<<"Inserire i metri Quadri-> ";
        cin >> V[i].metriQuadri;

        cout<<"Inserire il numero di bagni-> ";
        cin >> V[i].nBagni;

        cout<<"Inserire il prezzo-> ";
        cin >> V[i].prezzo;

        cin.ignore();
        cout<<"Inserire il comune-> ";
        getline(cin,V[i].comune);

        cout<<"Inserire numero di box-> ";
        cin >> V[i].box;

    }
}

void stampaVettore(appartamento V[], int D){
    for(int i=0; i<D; i++){
        cout<<"=====Appartamento "<<i+1<<"====="<<endl;
        cout<<"Numero di stanze: "<<V[i].nStanze<<endl;
        cout<<"Numero di metri quadri: "<<V[i].metriQuadri<<endl;
        cout<<"Numero di bagni: "<<V[i].nBagni<<endl;
        cout<<"Prezzo: "<<V[i].prezzo<<endl;
        cout<<"Comune: "<<V[i].comune<<endl;
        cout<<"Numero di box: "<<V[i].box<<endl;

    }
}

void ordinaPerPrezzo(appartamento V[], int D){
    float temp;
    for(int i=0; i<D; i++){
        for(int j=i+1; j<D; j++){
            if(V[i].prezzo>V[j].prezzo){
                temp=V[i].prezzo;
                V[i].prezzo=V[j].prezzo;
                V[j].prezzo=temp;
            }
        }
    }
}
