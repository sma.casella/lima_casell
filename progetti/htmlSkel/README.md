# Informazioni sullo sviluppatore

Nome: Casella Matteo

Istituto: it Manzetti

Anno: 2007

Mail: sma.casella@mail.scuole.vda.it

## Il programma di questo progetto

questo programma serve a creare un file html

## Esempio di output del programma

- - - - - - - - - 
$ ./output.exe main
il file non esiste, creo il file
.
.
.
il file e' stato creato

- - - - - - - - -

## Esempio di programma che viene generato

- - - - - - - - - - -

<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>
</head>
<body>
<h1>Ciao Mondo!</h1>
<p>L'informatico non si arrende MAI!!</p>
</body>
</html>

- - - - - - - - - - -