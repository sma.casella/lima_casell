# Informazioni sullo sviluppatore

Nome: Casella Matteo

Istituto: it Manzetti

Età: 16 anni

Mail: sma.casella@mail.scuole.vda.it

## Il programma main di questo progetto

questo programma serve a stampare a video un array di 3 alunni, 
i alunni vengono generati con dati casuali

## Esempio di output del programma

- - - - - - - - - - - - - - - 

======Alunni======
Nome: Mimmo
Cognome: Bianchi
Anno: 2006
Nome: Marco
Cognome: Verdi
Anno: 2005
Nome: Samuele
Cognome: Verdi
Anno: 2007

- - - - - - - - - - - - - -
