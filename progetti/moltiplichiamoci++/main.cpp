#include <iostream>
#include <ctime>
#include<iomanip>
using namespace std;
int getInt();
int casualeTra(int massimo, int minore);
int main(){
    srand((unsigned int)time(NULL));
    int n = casualeTra(6,2);
    for (int i=0; i<n; i++){
        for(int j=0; j<n; j++){
	  cout<<setfill('0')<<setw(3)<<i*j<<"|";
        }
        cout<<endl;
    }
    return 0;
}
int getInt(){
    int ans=rand()%7+1;
    return ans;
}
int casualeTra(int massimo, int minore){
    return rand()%(massimo-minore+1)+minore;
}
