#ifndef FUNCTION_H_
    #define FUNCTION_H_     //!qua prototipi e classi
    #include <iostream>     
    #include <string>

    using namespace std;

    class Counter{
    public:
        Counter():value(0){} //costruttore
        Counter(int n):value(n){}
        int getValue(){
            return value;
        }
        void increment(int n){
            value += n;
        }
    private:
        int value;
    };

    class Complex{      //!classe per i numeri complessi
    public:
        Complex(double a, double b):real(a), imag(b){}
        Complex():real(0), imag(0){}
        Complex(const Complex& z): real(z.real), imag(z.imag){}

        Complex add( Complex y, Complex z){
            return Complex (y.real + y.imag, z.real + z.imag);
        }

        Complex subtract(Complex y, Complex z){
            return Complex(y.real - y.imag, z.real - z.imag);
        }

        Complex multiply(Complex y, Complex z){
            return Complex(y.real * y.imag - z.real * z.imag, y.real * y.imag + z.real * z.imag);

        }

        Complex divide(Complex y, Complex z){
            return Complex((y.real * z.real + z.imag * y.imag)/(z.real * z.real + z.imag * z.imag),(y.imag * z.real - y.imag * z.imag)/(z.real * z.real + z.imag * z.imag));
        }

        bool compare(Complex y, Complex z){
            return (y.real == z.real && y.imag== z.imag);
        }

        void print() {
            cout<<"("<<real<<","<<imag<<")"<<endl;
        }

        double imagPart(){
            return imag;
        }

        double realPart(){
            return real;
        }
    private:
        int value;
        double real, imag;
    };

#endif // FUNCTION_H_
