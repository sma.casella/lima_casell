#include "function.h"
#include <iostream>
#include <ctime>
#include <cstdlib>

using namespace std;

int main(){
    const int DIM = 10;
    int mioVettore[DIM];
    system("cls");

    cout<<"vettore:"<<endl;
    riempiVettore(mioVettore, DIM);
    stampaVettore(mioVettore, DIM);
    cout<<"il minimo e': "<<minimo(mioVettore, DIM)<<endl;
    cout<<"il massimo e': "<<massimo(mioVettore, DIM)<<endl;
    cout<<"il media e': "<<media(mioVettore, DIM)<<endl;

    return 0;
}
