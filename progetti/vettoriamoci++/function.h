#ifndef FUNCTION_H
#define FUNCTION_H

void riempiVettore(int v[], const int D);
void stampaVettore(int v[], const int D);
int minimo(int v[], const int D);
int massimo(int v[], const int D);
float media(int v[], const int D);

#endif // FUNCTION_H
