# Informazioni sullo sviluppatore

Nome: Casella Matteo

Istituto: it Manzetti

Età: 16 anni

Mail: sma.casella@mail.scuole.vda.it

## Il programma main di questo progetto
questo programma serve a generare un numero casuale di punti in un quarto di circonferenza
nel quadrante del piano cartesiano in cui sia x che y sono positivi

## Esempio di output del programma
- - - - - - - - - 
dammi punti da generare:
2     
- - - - - - - - -


