# installazione del programma del progetto base

per installare il programma main.cpp cliccare sull'icona di download 
nella repository.

## Compilazione del programma main

per compilare il file main.cpp eseguire il seguente comando da linea di 
comando

- - - - - - - - - -
 
g++ -Wconversion -Wall main.cpp

- - - - - - - - - -

## Esecuzione del programma main

invece per eseguire il programma usare il seguente comando da linea di comando
- - - - - - - - - -

./a.exe

- - - - - - - - - -